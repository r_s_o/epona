Epona
=====


What
----

An Erlang library (a gen_server actually) to build and to control a Qt-based graphical user interface. It uses the `qt5_eport.bin` app, so build that one before toying with epona itself.

Why
---

Because I don't like wx. Respectfully, it's 2020.


How
---

Library source code is in `epona.erl`. Sample UI building and handling is in `demo_gs.erl`.


Run
---

    QT5_EPORT_DIR=~/Dev/epona/build rebar3 shell --apps epona

or just

    ./r.sh

Remember you must provide `QT5_EPORT_DIR` env var.


Who
---

r.s.o.aliev@gmail.com

