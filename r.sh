#!/bin/sh

# One must provide epona with the location of qt5_eport.bin, therefore QT5_EPORT_DIR.

rebar3 compile && QT5_EPORT_DIR=~/Dev/epona/build rebar3 shell --apps epona
