-module(demo_gs).
-author("ra").
-behaviour(gen_server).
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2]).
-include("ids.hrl").

% These defines are not really necessary, just practical.
-define(BUTTON_FIRST, <<"firstButton">>).
-define(BUTTON_SECOND, <<"secondButton">>).
-define(BUTTON_THIRD, <<"thirdButton">>).
-define(BUTTON_FOURTH, <<"fourthButton">>).
-define(BUTTON_POPUP_MENU1, <<"popupMenuButton1">>).
-define(BUTTON_POPUP_MENU2, <<"popupMenuButton2">>).
-define(BUTTON_POPUP_MENU3, <<"popupMenuButton3">>).
-define(EDIT_1, <<"edit1">>).
-define(EDIT_2, <<"edit2">>).
-define(BUTTON_DIALOG_1, <<"buttonDialog1">>).
-define(BUTTON_DIALOG_2, <<"buttonDialog2">>).
-define(LABEL_FROM_FORM_2, <<"labelFromForm2">>).

-define(MENU_POPUP, <<"popupMenu">>).

-define(MENU_CLOSE, <<"menuItemClose">>).
-define(MENU_ABOUT, <<"menuItemAbout">>).
-define(MENU_HELP, <<"menuItemHelp">>).

-define(FORM_1, <<"form1">>).
-define(FORM_2, <<"form2">>).
-define(EDIT_FORM_2, <<"editForm2">>).

% One can keep widgets' ids in the state too.
-record(state,
{
	layout_stacked :: binary() | undefined
}).
-type state() :: #state{}.


%% API

start_link() ->
	io:format("demo_gs:start_link~n"),
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


%% Callbacks

init([]) ->
	% Defer UI building.
	gen_server:cast(?MODULE, build_ui),
	{ok, #state{}}.

handle_call(_Request, _From, State = #state{}) -> {reply, ok, State}.

handle_cast(build_ui, State = #state{}) ->
	% Actually build UI.
	{noreply, buildUi(State)};
handle_cast(_Request, State = #state{}) ->
	{noreply, State}.

% 'Callbacks' for Qt events.
handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_FIRST}, State = #state{layout_stacked = Layout}) ->
	epona:property(?EPONA_EMPTY, Layout, {?EPONA_CURRENT_INDEX, <<"0">>}),
	{noreply, State};
handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_SECOND}, State = #state{layout_stacked = Layout}) ->
	epona:property(?EPONA_EMPTY, Layout, {?EPONA_CURRENT_INDEX, <<"1">>}),
	{noreply, State};
handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_THIRD}, State = #state{layout_stacked = Layout}) ->
	epona:property(?EPONA_EMPTY, Layout, {?EPONA_CURRENT_INDEX, <<"2">>}),
	{noreply, State};
handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_FOURTH}, State = #state{layout_stacked = Layout}) ->
	epona:property(Layout, {?EPONA_CURRENT_INDEX, <<"3">>}),
	{noreply, State};

handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_POPUP_MENU1}, State = #state{}) ->
	epona:popupMenu(?MENU_POPUP, ?BUTTON_POPUP_MENU1),
	{noreply, State};
handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_POPUP_MENU2}, State = #state{}) ->
	epona:popupMenu(?MENU_POPUP, ?BUTTON_POPUP_MENU2),
	{noreply, State};
handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_POPUP_MENU3}, State = #state{}) ->
	epona:popupMenu(?MENU_POPUP, ?BUTTON_POPUP_MENU3),
	{noreply, State};

handle_info({?EPONA_LINE_EDIT_TEXT_EDITED, ?EDIT_1, Text}, State = #state{}) ->
	epona:property(?EDIT_2, {?EPONA_TEXT, Text}),
	{noreply, State};
handle_info({?EPONA_LINE_EDIT_TEXT_EDITED, ?EDIT_2, Text}, State = #state{}) ->
	epona:property(?EDIT_1, {?EPONA_TEXT, Text}),
	{noreply, State};

handle_info({?EPONA_MENU_TRIGGERED, ?MENU_CLOSE}, State = #state{}) ->
	RequestId = <<"closeRequest">>,
	epona:questionDialog(self(), RequestId, "Close", "Really close?"),
	% Bad, bad practice. We want it nice and clean, true OTP way, right?
	receive
		{?EPONA_QUESTION_RESPONSE, RequestId, ?EPONA_YES} ->
			io:format("client's answer is YES... closing~n"),
			% Kinda redundunt here because of application:stop.
			% But you don't always want to stop the world.
			epona:close(),
			application:stop(epona);
		{?EPONA_QUESTION_RESPONSE, RequestId, ?EPONA_NO} ->
			io:format("client's answer is NO~n")
	end,
	{noreply, State};
handle_info({?EPONA_MENU_TRIGGERED, ?MENU_ABOUT}, State = #state{}) ->
	epona:informationDialog("About", "Erlang <> Qt5 demo application.\n\nhttps://bitbucket.org/r_s_o/"),
	{noreply, State};
handle_info({?EPONA_MENU_TRIGGERED, ?MENU_HELP}, State = #state{}) ->
	epona:warningDialog("Help", "Here is your help: don't panic."),
	{noreply, State};

handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_DIALOG_1}, State = #state{}) ->
	buildForm1(State),
	{noreply, State};
handle_info({?EPONA_BUTTON_CLICKED, ?BUTTON_DIALOG_2}, State = #state{}) ->
	buildForm2(State),
	{noreply, State};

handle_info({?EPONA_LINE_EDIT_TEXT_EDITED, ?EDIT_FORM_2, Text}, State = #state{}) ->
	epona:property(?LABEL_FROM_FORM_2, {?EPONA_TEXT, Text}),
	{noreply, State};

handle_info(_Info, State = #state{}) ->
	{noreply, State}.

terminate(_Reason, _State = #state{}) -> ok.


%% Utilities

-spec buildUi(state()) -> state().
buildUi(State) ->
	% Ids. Again, practical, not mandatory.
	LayoutMain = <<"mainLayout">>,

	LayoutLeft = <<"leftLayout">>,
	LabelFirst = <<"firstLabel">>,
	LabelSecond = <<"secondLabel">>,
	LabelThird = <<"thirdLabel">>,
	LabelFourth = <<"fourthLabel">>,
	ButtonFirst = ?BUTTON_FIRST,
	ButtonSecond = ?BUTTON_SECOND,
	ButtonThird = ?BUTTON_THIRD,
	ButtonFourth = ?BUTTON_FOURTH,

	LayoutStacked = <<"stackedLayout">>,

	WidgetFirst = <<"firstWidget">>,
	WidgetFirstLayout = epona:idToLayoutId(WidgetFirst), % Some automatics.
	Splitter = epona:newId(), % And here too, when we don't really need a meaningful id.

	WidgetSecond = <<"secondWidget">>,
	WidgetSecondLayout = epona:idToLayoutId(WidgetSecond),
	Button00 = <<"button00">>,
	Label01 = <<"label01">>,
	Label02 = <<"label02">>,
	Label10 = <<"label10">>,
	Button11 = <<"button11">>,
	Label12 = <<"label12">>,
	Label20 = <<"label20">>,
	Label21 = <<"label21">>,
	Button22 = <<"button22">>,

	WidgetThird = <<"thirdWidget">>,
	LayoutThird = <<"thirdLayout">>,
	Check1 = <<"check1">>,
	Check2 = <<"check2">>,
	Check3 = <<"check3">>,
	Combo1 = <<"combo1">>,
	Radio1 = <<"radio1">>,
	Radio2 = <<"radio2">>,
	Radio3 = <<"radio3">>,

	WidgetFourth = <<"fourthWidget">>,
	Tab1 = <<"tab1">>,
	Tab1Layout = epona:idToLayoutId(Tab1),
	List1 = <<"list1">>,
	List2 = <<"list2">>,
	Tab2 = <<"tab2">>,
	Tab2Layout = epona:idToLayoutId(Tab2),
	Edit1 = ?EDIT_1,
	Edit2 = ?EDIT_2,
	Tab3 = <<"tab3">>,
	Tab3Layout = epona:idToLayoutId(Tab3),
	LabelDialogs = <<"labelDialog1">>,
	ButtonDialog1 = ?BUTTON_DIALOG_1,
	ButtonDialog2 = ?BUTTON_DIALOG_2,
	LabelFromForm2 = ?LABEL_FROM_FORM_2,

	MenuBar = <<"mainMenuBar">>,
	SubmenuFile = <<"submenuFile">>,
	MenuItemClose = ?MENU_CLOSE,
	SubmenuHelp = <<"submenuHelp">>,
	MenuItemAbout = ?MENU_ABOUT,
	MenuItemHelp = ?MENU_HELP,

	Ui =
		[
			% General MainWindow parameters.
			{property, [?EPONA_EMPTY, ?EPONA_MAIN_WINDOW, {?EPONA_WINDOW_TITLE, <<"Epona demo">>}]},
			{resize, [800, 600]},
			{horizontalLayout, [?EPONA_EMPTY, LayoutMain, ?EPONA_NO_LAYOUT]},
			% Most of the standard Qt properties are supported, at least partially.
			% Just see Qt widgets' documentation.
			{property, [?EPONA_EMPTY, LayoutMain, {?EPONA_SPACING, <<"0">>}]},

			% Main layout.
			{formLayout, [?EPONA_MAIN_WINDOW, LayoutLeft, LayoutMain]},
			% Creating widgets.
			{label, [LabelFirst, <<"Splitters, buttons, popup menus">>]},
			{label, [LabelSecond, <<"Grid layout and some buttons">>]},
			{label, [LabelThird, <<"GroupBox">>]},
			{label, [LabelFourth, <<"TabWidget, lists, edits, stretchers">>]},
			{pushButton, [ButtonFirst, <<"Switch to first">>]},
			{pushButton, [ButtonSecond, <<"Switch to second">>]},
			{pushButton, [ButtonThird, <<"Switch to third">>]},
			{pushButton, [ButtonFourth, <<"Switch to fourth">>]},
			% Placing created widgets into layouts.
			{toFormLayout, [LabelFirst, ButtonFirst, LayoutLeft]},
			{toFormLayout, [LabelSecond, ButtonSecond, LayoutLeft]},
			{toFormLayout, [LabelThird, ButtonThird, LayoutLeft]},
			{toFormLayout, [LabelFourth, ButtonFourth, LayoutLeft]},

			% Rightmost layout with 'pages'.
			{stackedLayout, [?EPONA_EMPTY, LayoutStacked, LayoutMain]},

			% First 'page': a few buttons, nothing special.
			{widget, [WidgetFirst, horizontal_layout]},
			{toStackedLayout, [WidgetFirst, LayoutStacked]},
			% Creating and placing can be combined into a single pass.
			{splitter, [Splitter, horizontal], {toBoxLayout, [WidgetFirstLayout]}},
			{pushButton, [?BUTTON_POPUP_MENU1, <<"Popup menu">>], {toSplitter, [Splitter]}},
			{pushButton, [?BUTTON_POPUP_MENU2, <<"Popup menu, again">>], {toSplitter, [Splitter]}},
			{pushButton, [?BUTTON_POPUP_MENU3, <<"YAPM">>], {toSplitter, [Splitter]}},

			% Second 'page': grid layout demo.
			{widget, [WidgetSecond, grid_layout]},
			{toStackedLayout, [WidgetSecond, LayoutStacked]},
			{pushButton, [Button00, <<"(0, 0)">>], {toGridLayout, [WidgetSecondLayout, 0, 0]}},
			{label, [Label01, <<"(0, 1)">>], {toGridLayout, [WidgetSecondLayout, 0, 1]}},
			{label, [Label02, <<"(0, 2)">>], {toGridLayout, [WidgetSecondLayout, 0, 2]}},
			{label, [Label10, <<"(1, 0)">>], {toGridLayout, [WidgetSecondLayout, 1, 0]}},
			{pushButton, [Button11, <<"(1, 1)">>], {toGridLayout, [WidgetSecondLayout, 1, 1]}},
			{label, [Label12, <<"(1, 2)">>], {toGridLayout, [WidgetSecondLayout, 1, 2]}},
			{label, [Label20, <<"(2, 0)">>], {toGridLayout, [WidgetSecondLayout, 2, 0]}},
			{label, [Label21, <<"(2, 1)">>], {toGridLayout, [WidgetSecondLayout, 2, 1]}},
			{pushButton, [Button22, <<"(2, 2)">>], {toGridLayout, [WidgetSecondLayout, 2, 2]}},

			{property, [Label01, {?EPONA_FRAME_SHAPE, <<"QFrame::Box">>}]},
			{property, [Label02, {?EPONA_FRAME_SHAPE, <<"QFrame::Box">>}]},
			{property, [Label10, {?EPONA_FRAME_SHAPE, <<"QFrame::Box">>}]},
			{property, [Label12, {?EPONA_FRAME_SHAPE, <<"QFrame::Box">>}]},
			{property, [Label20, {?EPONA_FRAME_SHAPE, <<"QFrame::Box">>}]},
			{property, [Label21, {?EPONA_FRAME_SHAPE, <<"QFrame::Box">>}]},

			% Third 'page': buttons, combos, buttons again...
			{groupBox, [WidgetThird, <<"GroupBox">>, no_layout]},
			{toStackedLayout, [WidgetThird, LayoutStacked]},
			{verticalLayout, [LayoutThird, WidgetThird]}, % Lets make one manually for a change.
			{checkBox, [Check1, <<"First checkBox">>]},
			{checkBox, [Check2, <<"Second checkBox">>]},
			{checkBox, [Check3, <<"Third checkBox">>]},
			{comboBox, [Combo1, [<<"Aaa">>, <<"Bbb">>, <<"Ccc">>]]},
			{radioButton, [Radio1, <<"Radio1">>]},
			{radioButton, [Radio2, <<"Radio2">>]},
			{radioButton, [Radio3, <<"Radio3">>]},
			{toBoxLayout, [Check1, LayoutThird]},
			{toBoxLayout, [Check2, LayoutThird]},
			{toBoxLayout, [Check3, LayoutThird]},
			{toBoxLayout, [Combo1, LayoutThird]},
			{toBoxLayout, [Radio1, LayoutThird]},
			{toBoxLayout, [Radio2, LayoutThird]},
			{toBoxLayout, [Radio3, LayoutThird]},

			% Fourth 'page': tabWidget.
			{tabWidget, [WidgetFourth]},
			{toStackedLayout, [WidgetFourth, LayoutStacked]},
			{widget, [Tab1, vertical_layout], {toTabWidget, [WidgetFourth, <<"Tab1">>]}},
			{listWidget, [List1, [<<"Aaa">>, <<"Bbb">>, <<"Ccc">>]], {toBoxLayout, [Tab1Layout]}},
			{listWidget, [List2, [<<"Ddd">>, <<"Eee">>, <<"Fff">>]], {toBoxLayout, [Tab1Layout]}},
			{widget, [Tab2, vertical_layout], {toTabWidget, [WidgetFourth, <<"Tab2">>]}},
			{label, [epona:newId(), <<"Try typing here">>], {toBoxLayout, [Tab2Layout]}},
			{stretcher, [Tab2Layout]},
			{lineEdit, [Edit1, [<<"">>]], {toBoxLayout, [Tab2Layout]}},
			{lineEdit, [Edit2, [<<"">>]], {toBoxLayout, [Tab2Layout]}},
			{widget, [Tab3, horizontal_layout], {toTabWidget, [WidgetFourth, <<"Tab3">>]}},
			{label, [LabelDialogs, <<"Try some dialogs">>], {toBoxLayout, [Tab3Layout]}},
			{stretcher, [Tab3Layout]},
			{pushButton, [ButtonDialog1, <<"One">>], {toBoxLayout, [Tab3Layout]}},
			{pushButton, [ButtonDialog2, <<"Two">>], {toBoxLayout, [Tab3Layout]}},
			{label, [LabelFromForm2, <<"">>], {toBoxLayout, [Tab3Layout]}},

			% Menu bar.
			{menuBar, [MenuBar]},
			{submenu, [SubmenuFile, <<"&File">>, MenuBar]},
			{menuItem, [MenuItemClose, <<"&Close">>, SubmenuFile]},
			{submenu, [SubmenuHelp, <<"&Help">>, MenuBar]},
			{menuItem, [MenuItemAbout, <<"&About">>, SubmenuHelp]},
			{menuSeparator, [epona:newId(), SubmenuHelp]},
			{menuItem, [MenuItemHelp, <<"&Help">>, SubmenuHelp]},

			% Popup menu.
			{menu, [?MENU_POPUP]},
			{menuItem, [epona:newId(), <<"Sample item 1">>, ?MENU_POPUP]},
			{menuSeparator, [epona:newId(), ?MENU_POPUP]},
			{menuItem, [epona:newId(), <<"Sample item 2">>, ?MENU_POPUP]},

			% Finally, subscribing to events.
			{subscribe, [self(), ButtonFirst]},
			{subscribe, [self(), ButtonSecond]},
			{subscribe, [self(), ButtonThird]},
			{subscribe, [self(), ButtonFourth]},
			{subscribe, [self(), ?BUTTON_POPUP_MENU1]},
			{subscribe, [self(), ?BUTTON_POPUP_MENU2]},
			{subscribe, [self(), ?BUTTON_POPUP_MENU3]},
			{subscribe, [self(), Edit1]},
			{subscribe, [self(), Edit2]},
			{subscribe, [self(), ButtonDialog1]},
			{subscribe, [self(), ButtonDialog2]},
			{subscribe, [self(), MenuItemClose]},
			{subscribe, [self(), MenuItemAbout]},
			{subscribe, [self(), MenuItemHelp]}
		],

	% We build the UI in one pass. But it could be constructed step by step,
	% by direct calling of epona:whatever functions.
	epona:batch(Ui),
	State#state
	{
		layout_stacked = LayoutStacked
	}.

buildForm1(State) ->
	Form = ?FORM_1,
	Layout = epona:idToLayoutId(Form),
	Label = <<"label1">>,

	% Not every window is the MainWindow.
	% Notice one additional argument here - 'Form'.
	Ui =
		[
			{form, [Form, <<"Dialog 1">>]},
			{verticalLayout, [Form, Layout, Form]},
			{label, [Label, <<"Label for dialog 1, nothing to see here">>], {toBoxLayout, [Layout]}}
		],

	epona:batch(Ui),
	State.

buildForm2(State) ->
	Form = ?FORM_2,
	Layout = epona:idToLayoutId(Form),
	Label = <<"label1">>,
	Edit = ?EDIT_FORM_2,

	Ui =
		[
			{form, [Form, <<"Dialog 2">>]},
			{horizontalLayout, [Form, Layout, Form]},
			{label, [Label, <<"Type here">>], {toBoxLayout, [Layout]}},
			{lineEdit, [Edit, [<<"">>]], {toBoxLayout, [Layout]}},

			% And we can freely pass properties between forms.
			{subscribe, [self(), Edit]}
		],

	epona:batch(Ui),
	State.

