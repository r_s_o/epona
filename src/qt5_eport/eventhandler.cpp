#include "eventhandler.h"

#include <QMouseEvent>
#include <iostream>
using namespace std;

bool EventHandler::eventFilter(QObject* obj, QEvent* event)
{
   if(event->type() == QEvent::MouseButtonPress)
   {
      auto mouseEvent = static_cast<QMouseEvent*>(event);
      if(mouseEvent->button() == Qt::RightButton)
      {
         emit rightPressed(parent()->objectName());
         return true;
      }
   }
   else
      return QObject::eventFilter(obj, event);
   return false;
}
