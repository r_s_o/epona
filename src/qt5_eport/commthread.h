#ifndef COMMTHREAD_H
#define COMMTHREAD_H

#include <QThread>
#include <QJsonObject>
#include <QJsonDocument>

#include <string>
#include <map>
#include <tuple>
#include <functional>
#include <optional>
#include <exception>
#include <initializer_list>

#include "utils.h"

// Convenience.
using Json = QString;

/** Communication thread for Erlang <-> port interop. */
class CommThread: public QThread
{
   Q_OBJECT

private:
   /** A function to process a single json query. */
   using Response = std::function<void(Json)>;
   /** A bunch of responses, aligned by command prefix ("$pushButton" etc). */
   using Responses = std::map<QString, Response>;

   /** The method to find a corresponding response function. */
   std::optional<Response> response(const Responses& r, QString key)
   {
      auto ri=r.find(key);
      return r.end()!=ri ? std::optional<Response>{ri->second} : std::nullopt;
   }

   // Convenience.
   using json_exception = std::exception;

private:
   std::tuple<QString, QString> split(QString s);
   QJsonDocument convert(Json json);
   std::tuple<QJsonValue>
   extract(Json json, QString id1);
   std::tuple<QJsonValue, QJsonValue>
   extract(Json json, QString id1, QString id2);
   std::tuple<QJsonValue, QJsonValue, QJsonValue>
   extract(Json json, QString id1, QString id2, QString id3);
   std::tuple<QJsonValue, QJsonValue, QJsonValue, QJsonValue>
   extract(Json json, QString id1, QString id2, QString id3, QString id4);
   std::tuple<QJsonValue, QJsonValue, QJsonValue, QJsonValue, QJsonValue>
   extract(Json json, QString id1, QString id2, QString id3, QString id4, QString id5);
   // Ok, this has to stop.

   QString toJsonString(std::initializer_list<QPair<QString, QJsonValue> > args)
   {
      return QString(QJsonDocument{QJsonObject{args}}.toJson(QJsonDocument::Compact));
   }

public:
   CommThread();

   void run() override;

signals:
   void e_close();

   void e_information(QString title, QString text);
   void e_warning(QString title, QString text);
   void e_question(QString id, QString title, QString text);

   void e_form(QString id, QString title);

   void e_horizontalLayout(QString formId, QString id, QString parentId);
   void e_verticalLayout(QString formId, QString id, QString parentId);
   void e_formLayout(QString formId, QString id, QString parentId);
   void e_gridLayout(QString formId, QString id, QString parentId);
   void e_stackedLayout(QString formId, QString id, QString parentId);
   void e_stretcher(QString layoutId);

   void e_checkBox(QString id, QString text);
   void e_comboBox(QString id, QStringList items);
   void e_groupBox(QString id, QString title);
   void e_label(QString id, QString text);
   void e_lineEdit(QString id, QString text);
   void e_listWidget(QString id, QStringList items);
   void e_pushButton(QString id, QString text);
   void e_radioButton(QString id, QString text);
   void e_splitter(QString id, QString orientation);
   void e_tabWidget(QString id);
   void e_widget(QString id);

   void e_menu(QString id);
   void e_submenu(QString id, QString text, QString parentId);
   void e_menuItem(QString id, QString text, QString menuId);
   void e_menuSeparator(QString id, QString menuId);
   void e_menuBar(QString id);
   void e_popupMenu(QString menuId, QString formId, QString widgetId);

   void e_toBoxLayout(QString formId, QString id, QString layoutId);
   void e_toFormLayout(QString formId, QString labelId, QString fieldId, QString layoutId);
   void e_toGridLayout(QString formId, QString id, QString layoutId, int row, int column);
   void e_toStackedLayout(QString formId, QString id, QString layoutId);
   void e_toTabWidget(QString formId, QString id, QString tabWidgetId, QString label);
   void e_toSplitter(QString formId, QString id, QString splitterId);

   void e_setProperty(QString formId, QString id, QString name, QString value);
   void e_resize(QString formId, int width, int height);
   void e_maximize(QString formId);

public slots:
   // Notifications
   //TODO Do we really need it here, or is it better to just use main()?
   void e_questionResponse(QString id, QString response)
   {
      cerr<<":: e_questionResponse: id: "<<id<<", response: "<<response<<endl<<flush;
      cout<<
             "$questionResponse {\"id\":\""<<id<<
             "\",\"value\":\""<<response<<"\"}"<<endl<<flush;
   }

   void e_buttonClicked(QString id)
   {
      cerr<<":: "<<id<<" clicked"<<endl<<flush;
      cout<<"$buttonClicked {\"id\":\""<<id<<"\"}"<<endl<<flush;
   }
   void e_buttonPressed(QString id)
   {
      cerr<<":: "<<id<<" pressed"<<endl<<flush;
      cout<<"$buttonPressed {\"id\":\""<<id<<"\"}"<<endl<<flush;
   }
   void e_buttonReleased(QString id)
   {
      cerr<<":: "<<id<<" released"<<endl<<flush;
      cout<<"$buttonReleased {\"id\":\""<<id<<"\"}"<<endl<<flush;
   }
   void e_buttonToggled(QString id, bool checked)
   {
      cerr<<":: "<<id<<" toggled"<<endl<<flush;
      cout<<
             "$buttonToggled {\"id\":\""<<id<<
             "\",\"checked\":\""<<checked<<"\"}"<<endl<<flush;
   }
   void e_buttonRightPressed(QString id)
   {
      cerr<<":: "<<id<<" rightPressed"<<endl<<flush;
      cout<<"$buttonRightPressed {\"id\":\""<<id<<"\"}"<<endl<<flush;
   }

   void e_checkBoxStateChanged(QString id, int state)
   {
      cerr<<":: "<<id<<" state changed"<<endl<<flush;
      cout<<
             "$checkBoxStateChanged {\"id\":\""<<id<<
             "\",\"state\":\""<<state<<"\"}"<<endl<<flush;
   }

   void e_comboBoxCurrentIndexChanged(QString id, int index)
   {
      cerr<<":: "<<id<<" current index changed"<<endl<<flush;
      QString s=toJsonString({{"id", id}, {"index", QString::number(index)}});
      cout<<"$comboBoxCurrentIndexChanged "<<s<<endl<<flush;
   }
   void e_comboBoxCurrentTextChanged(QString id, const QString &text)
   {
      cerr<<":: "<<id<<" current text changed"<<endl<<flush;
      QString s=toJsonString({{"id", id}, {"text", text}});
      cout<<"$comboBoxCurrentTextChanged "<<s<<endl<<flush;
   }
   void e_comboBoxEditTextChanged(QString id, const QString &text)
   {
      cerr<<":: "<<id<<" text changed"<<endl<<flush;
      QString s=toJsonString({{"id", id}, {"text", text}});
      cout<<"$comboBoxEditTextChanged "<<s<<endl<<flush;
   }

   void e_lineEditTextEdited(QString id, QString text)
   {
      cerr<<":: "<<id<<" edited"<<endl<<flush;
      QString s=toJsonString({{"id", id}, {"text", text}});
      cout<<"$lineEditTextEdited "<<s<<endl<<flush;
   }

   void e_tabWidgetCurrentChanged(QString id, int index)
   {
      cerr<<":: "<<id<<" current changed"<<endl<<flush;
      cout<<
             "$tabWidgetCurrentChanged {\"id\":\""<<id<<
             "\",\"index\":\""<<index<<"\"}"<<endl<<flush;
   }
   void e_tabWidgetTabBarClicked(QString id, int index)
   {
      cerr<<":: "<<id<<" tab bar clicked"<<endl<<flush;
      cout<<
             "$tabWidgetTabBarClicked {\"id\":\""<<id<<
             "\",\"index\":\""<<index<<"\"}"<<endl<<flush;
   }
   void e_tabWidgetTabBarDoubleClicked(QString id, int index)
   {
      cerr<<":: "<<id<<" tab bar double clicked"<<endl<<flush;
      cout<<
             "$tabWidgetTabBarDoubleClicked {\"id\":\""<<id<<
             "\",\"index\":\""<<index<<"\"}"<<endl<<flush;
   }
   void e_tabWidgetTabCloseRequested(QString id, int index)
   {
      cerr<<":: "<<id<<" tab close requested"<<endl<<flush;
      cout<<
             "$tabWidgetTabCloseRequested {\"id\":\""<<id<<
             "\",\"index\":\""<<index<<"\"}"<<endl<<flush;
   }

   void e_menuTriggered(QString id)
   {
      cerr<<":: "<<id<<" menu triggered"<<endl<<flush;
      cout<<"$menuTriggered {\"id\":\""<<id<<"\"}"<<endl<<flush;
   }

}; // class CommThread

#endif // COMMTHREAD_H
