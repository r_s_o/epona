#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVariant>

#include <QDialog>
#include <QMessageBox>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QGridLayout>
#include <QStackedLayout>

#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QSplitter>
#include <QTabWidget>

#include <QMenu>
#include <QAction>
#include <QMenuBar>

#include <iostream>
using namespace std;

#include "ids.h"
#include "utils.h"
#include "eventhandler.h"

namespace Ui {
class MainWindow;
}

class MainWindow: public QMainWindow
{
   Q_OBJECT

public:
   explicit MainWindow(QWidget* parent=nullptr);
   ~MainWindow();

protected:
   void changeEvent(QEvent* e);

   QWidget* findFormById(QString formId);
   QObject* findById(QWidget* form, QString id);
   bool setParentById(QLayout* l, QString formId, QString parentId);

public slots:
   void e_close()
   {
      close();
   }

   void e_form(QString id, QString title)
   {
      auto d = new QDialog(this);
      d->setAttribute(Qt::WA_DeleteOnClose);
      d->setObjectName(id);
      d->setWindowTitle(title);
      d->setModal(true);
      d->show();
   }

   void e_information(QString title, QString text)
   {
      QMessageBox::information(this, title, text);
   }
   void e_warning(QString title, QString text)
   {
      QMessageBox::warning(this, title, text);
   }
   void e_question(QString id, QString title, QString text)
   {
      cerr<<"::: e_question: "<<id<<endl<<flush;
      switch(QMessageBox::question(this, title, text))
      {
      case  QMessageBox::Ok      : emit e_questionResponse(id, "ok"); break;
      case  QMessageBox::Cancel  : emit e_questionResponse(id, "cancel"); break;
      case  QMessageBox::Yes     : emit e_questionResponse(id, "yes"); break;
      case  QMessageBox::No      : emit e_questionResponse(id, "no"); break;
      default:
         emit e_questionResponse(id, "");
         break;
      }
   }

   void e_horizontalLayout(QString formId, QString id, QString parentId)
   {
      auto hl = new QHBoxLayout();
      hl->setObjectName(id);
      setParentById(hl, formId, parentId);
   }
   void e_verticalLayout(QString formId, QString id, QString parentId)
   {
      auto vl = new QVBoxLayout();
      vl->setObjectName(id);
      setParentById(vl, formId, parentId);
   }
   void e_formLayout(QString formId, QString id, QString parentId)
   {
      auto fl = new QFormLayout();
      fl->setObjectName(id);
      setParentById(fl, formId, parentId);
   }
   void e_gridLayout(QString formId, QString id, QString parentId)
   {
      auto gl = new QGridLayout();
      gl->setObjectName(id);
      setParentById(gl, formId, parentId);
   }
   void e_stackedLayout(QString formId, QString id, QString parentId)
   {
      auto sl = new QStackedLayout();
      sl->setObjectName(id);
      setParentById(sl, formId, parentId);
   }
   void e_stretcher(QString layoutId);

   void e_checkBox(QString id, QString text)
   {
      auto cb = new QCheckBox(text, this);
      cb->setObjectName(id);
      connect(cb, &QAbstractButton::clicked, this, &MainWindow::buttonClicked);
      connect(cb, &QAbstractButton::pressed, this, &MainWindow::buttonPressed);
      connect(cb, &QAbstractButton::released, this, &MainWindow::buttonReleased);
      connect(cb, &QAbstractButton::toggled, this, &MainWindow::buttonToggled);
      connect(cb, &QCheckBox::stateChanged, this, &MainWindow::checkBoxStateChanged);
   }
   void e_comboBox(QString id, QStringList items)
   {
      auto cb=new QComboBox(this);
      cb->setObjectName(id);
      for(auto item: items)
         cb->addItem(item);
      connect(cb, QOverload<int>::of(&QComboBox::currentIndexChanged),
              this, &MainWindow::comboBoxCurrentIndexChanged);
      connect(cb, &QComboBox::currentTextChanged, this, &MainWindow::comboBoxCurrentTextChanged);
      connect(cb, &QComboBox::editTextChanged, this, &MainWindow::comboBoxEditTextChanged);
   }
   void e_groupBox(QString id, QString title)
   {
      auto gb=new QGroupBox(title, this);
      gb->setObjectName(id);
   }
   void e_label(QString id, QString text)
   {
      auto l=new QLabel(text, this);
      l->setObjectName(id);
   }
   void e_lineEdit(QString id, QString text)
   {
      auto le=new QLineEdit(text, this);
      le->setObjectName(id);
      connect(le, &QLineEdit::textEdited, this, &MainWindow::lineEditTextEdited);
   }
   void e_listWidget(QString id, QStringList items)
   {
      auto lw=new QListWidget(this);
      lw->setObjectName(id);
      for(auto item: items)
         lw->addItem(item);
   }
   void e_pushButton(QString id, QString text)
   {
      auto pb=new QPushButton(text, this);
      pb->setObjectName(id);
      connect(pb, &QAbstractButton::clicked, this, &MainWindow::buttonClicked);
      connect(pb, &QAbstractButton::pressed, this, &MainWindow::buttonPressed);
      connect(pb, &QAbstractButton::released, this, &MainWindow::buttonReleased);
      connect(pb, &QAbstractButton::toggled, this, &MainWindow::buttonToggled);

      EventHandler* h = new EventHandler(pb);
      connect(h, &EventHandler::rightPressed, this, &MainWindow::buttonRightPressed);
      pb->installEventFilter(h);
   }
   void e_radioButton(QString id, QString text)
   {
      auto rb = new QRadioButton(text, this);
      rb->setObjectName(id);
      connect(rb, &QAbstractButton::clicked, this, &MainWindow::buttonClicked);
      connect(rb, &QAbstractButton::pressed, this, &MainWindow::buttonPressed);
      connect(rb, &QAbstractButton::released, this, &MainWindow::buttonReleased);
      connect(rb, &QAbstractButton::toggled, this, &MainWindow::buttonToggled);
   }
   void e_splitter(QString id, QString orientation)
   {
      auto s = new QSplitter(ID_VERTICAL==orientation ? Qt::Vertical : Qt::Horizontal, this);
      s->setObjectName(id);
   }
   void e_tabWidget(QString id)
   {
      auto tw=new QTabWidget(this);
      tw->setObjectName(id);
      connect(tw, &QTabWidget::currentChanged, this, &MainWindow::tabWidgetCurrentChanged);
      connect(tw, &QTabWidget::tabBarClicked, this, &MainWindow::tabWidgetTabBarClicked);
      connect(tw, &QTabWidget::tabBarDoubleClicked, this, &MainWindow::tabWidgetTabBarDoubleClicked);
      connect(tw, &QTabWidget::tabCloseRequested, this, &MainWindow::tabWidgetTabCloseRequested);
   }
   void e_widget(QString id)
   {
      auto w = new QWidget(this);
      w->setObjectName(id);
   }

   void e_menu(QString id)
   {
      auto m = new QMenu(this);
      m->setObjectName(id);
      connect(m, &QMenu::triggered, this, &MainWindow::menuTriggered);
   }
   void e_submenu(QString id, QString text, QString parentId)
   {
      auto submenu = new QMenu(text, this);
      submenu->setObjectName(id);
      if( auto menu = qobject_cast<QMenu*>(findById(this, parentId)) )
         menu->addMenu(submenu);
      else if( auto menuBar = qobject_cast<QMenuBar*>(findById(this, parentId)) )
         menuBar->addMenu(submenu);
      else
         cerr<<":: submenu: can't find the parent: "<<parentId<<endl<<flush;
      //TODO Should we delete submenu?
   }
   void e_menuItem(QString id, QString text, QString menuId)
   {
      if( auto menu = qobject_cast<QMenu*>(findById(this, menuId)) )
      {
         auto action = menu->addAction(text);
         action->setObjectName(id);
         return;
      }
      cerr<<":: menuItem: can't find parent menu: "<<menuId<<endl<<flush;
   }
   void e_menuSeparator(QString id, QString menuId)
   {
      if( auto menu = qobject_cast<QMenu*>(findById(this, menuId)) )
      {
         auto action = menu->addSeparator();
         action->setObjectName(id);
         return;
      }
      cerr<<":: menuSeparator: can't find parent menu: "<<menuId<<endl<<flush;
   }
   void e_menuBar(QString id)
   {
      auto menuBar = new QMenuBar(this);
      menuBar->setObjectName(id);
      this->setMenuBar(menuBar);
      connect(menuBar, &QMenuBar::triggered, this, &MainWindow::menuTriggered);
   }
   void e_popupMenu(QString menuId, QString formId, QString widgetId)
   {
      if( auto form = findFormById(formId) )
      {
         auto menu = qobject_cast<QMenu*>(findById(form, menuId));
         auto widget = qobject_cast<QWidget*>(findById(form, widgetId));
         if(menu && widget)
         {
            QPoint p(0, widget->height());
            menu->popup( widget->mapToGlobal(p) );
         }
      }
      cerr<<":: popupMenu gone bad: "<<menuId<<", "<<formId<<", "<<widgetId<<endl<<flush;
   }

   void e_toBoxLayout(QString formId, QString id, QString layoutId);
   void e_toFormLayout(QString formId, QString labelId, QString fieldId, QString layoutId);
   void e_toGridLayout(QString formId, QString id, QString layoutId, int row, int column);
   void e_toStackedLayout(QString formId, QString id, QString layoutId);
   void e_toTabWidget(QString formId, QString id, QString tabWidgetId, QString label);
   void e_toSplitter(QString formId, QString id, QString splitterId);

   void e_setProperty(QString formId, QString id, QString name, QString value)
   {
      auto form = findFormById(formId);
      if( auto object = findById(form, id) )
      {
         object->setProperty(name.toLatin1(), QVariant(value));
         return;
      }
      cerr<<"setProperty: no such object: "<<id<<endl<<flush;
   }
   void e_resize(QString formId, int width, int height)
   {
      if( auto form = findFormById(formId) )
         form->resize(width, height);
   }
   void e_maximize(QString formId)
   {
      findFormById(formId)->showMaximized();
   }

signals:
   void e_questionResponse(QString Id, QString response);

   void e_buttonClicked(QString id);
   void e_buttonPressed(QString id);
   void e_buttonReleased(QString id);
   void e_buttonToggled(QString id, bool checked);
   void e_buttonRightPressed(QString id);

   void e_checkBoxStateChanged(QString id, int state);

   void e_comboBoxCurrentIndexChanged(QString id, int index);
   void e_comboBoxCurrentTextChanged(QString id, const QString &text);
   void e_comboBoxEditTextChanged(QString id, const QString &text);

   void e_lineEditTextEdited(QString id, QString text);

   void e_tabWidgetCurrentChanged(QString id, int index);
   void e_tabWidgetTabBarClicked(QString id, int index);
   void e_tabWidgetTabBarDoubleClicked(QString id, int index);
   void e_tabWidgetTabCloseRequested(QString id, int index);

   void e_menuTriggered(QString id);

private slots:
   // QAbstractButton
   void buttonClicked(bool /*checked*/)
   {
      if( auto button = qobject_cast<QAbstractButton*>(sender()) )
         emit e_buttonClicked(button->objectName());
   }
   void buttonPressed()
   {
      if( auto button = qobject_cast<QAbstractButton*>(sender()) )
         emit e_buttonPressed(button->objectName());
   }
   void buttonReleased()
   {
      if( auto button = qobject_cast<QAbstractButton*>(sender()) )
         emit e_buttonReleased(button->objectName());
   }
   void buttonToggled(bool checked)
   {
      if( auto button = qobject_cast<QAbstractButton*>(sender()) )
         emit e_buttonToggled(button->objectName(), checked);
   }
   void buttonRightPressed(QString id)
   {
      emit e_buttonRightPressed(id);
   }
   // QCheckBox
   void checkBoxStateChanged(int state)
   {
      if( auto box = qobject_cast<QCheckBox*>(sender()) )
         emit e_checkBoxStateChanged(box->objectName(), state);
   }
   // QComboBox
   void comboBoxCurrentIndexChanged(int index)
   {
      if( auto box = qobject_cast<QComboBox*>(sender()) )
         emit e_comboBoxCurrentIndexChanged(box->objectName(), index);
   }
   void comboBoxCurrentTextChanged(const QString &text)
   {
      if( auto box = qobject_cast<QComboBox*>(sender()) )
         emit e_comboBoxCurrentTextChanged(box->objectName(), text);
   }
   void comboBoxEditTextChanged(const QString &text)
   {
      if( auto box = qobject_cast<QComboBox*>(sender()) )
         emit e_comboBoxEditTextChanged(box->objectName(), text);
   }
   // QLineEdit
   void lineEditTextEdited(const QString &text)
   {
      if( auto edit = qobject_cast<QLineEdit*>(sender()) )
         emit e_lineEditTextEdited(edit->objectName(), text);
   }
   // QTabWidget
   void tabWidgetCurrentChanged(int index)
   {
      if( auto tw = qobject_cast<QTabWidget*>(sender()) )
         emit e_tabWidgetCurrentChanged(tw->objectName(), index);
   }
   void tabWidgetTabBarClicked(int index)
   {
      if( auto tw = qobject_cast<QTabWidget*>(sender()) )
         emit e_tabWidgetTabBarClicked(tw->objectName(), index);
   }
   void tabWidgetTabBarDoubleClicked(int index)
   {
      if( auto tw = qobject_cast<QTabWidget*>(sender()) )
         emit e_tabWidgetTabBarDoubleClicked(tw->objectName(), index);
   }
   void tabWidgetTabCloseRequested(int index)
   {
      if( auto tw = qobject_cast<QTabWidget*>(sender()) )
         emit e_tabWidgetTabCloseRequested(tw->objectName(), index);
   }
   // QMenu/QMenuBar
   void menuTriggered(QAction* action)
   {
      if(action)
         emit e_menuTriggered(action->objectName());
   }

private:
   Ui::MainWindow* ui;
};

#endif // MAINWINDOW_H
