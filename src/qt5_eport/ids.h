#ifndef IDS_H
#define IDS_H

const QString ID_EXIT = "$exit";

const QString ID_CLOSE = "$close";
const QString ID_FORM = "$form";

const QString ID_INFORMATION_BOX = "$informationDialog";
const QString ID_WARNING_BOX = "$warningDialog";
const QString ID_QUESTION_BOX = "$questionDialog";

const QString ID_HORIZONTAL_LAYOUT = "$horizontalLayout";
const QString ID_VERTICAL_LAYOUT = "$verticalLayout";
const QString ID_FORM_LAYOUT = "$formLayout";
const QString ID_GRID_LAYOUT = "$gridLayout";
const QString ID_STACKED_LAYOUT = "$stackedLayout";

const QString ID_STRETCHER = "$stretcher";

const QString ID_GROUP_BOX = "$groupBox";
const QString ID_CREATE_LABEL = "$label";
const QString ID_LINE_EDIT = "$lineEdit";
const QString ID_LIST_WIDGET = "$listWidget";
const QString ID_CHECK_BOX = "$checkBox";
const QString ID_COMBO_BOX = "$comboBox";
const QString ID_PUSH_BUTTON = "$pushButton";
const QString ID_RADIO_BUTTON = "$radioButton";
const QString ID_SPLITTER = "$splitter";
const QString ID_TAB_WIDGET = "$tabWidget";
const QString ID_WIDGET = "$widget";

const QString ID_MENU = "$menu";
const QString ID_SUBMENU = "$submenu";
const QString ID_MENU_ITEM = "$menuItem";
const QString ID_MENU_SEPARATOR = "$menuSeparator";
const QString ID_MENU_BAR = "$menuBar";
const QString ID_POPUP_MENU = "$popupMenu";

const QString ID_TO_BOX_LAYOUT = "$toBoxLayout";
const QString ID_TO_FORM_LAYOUT = "$toFormLayout";
const QString ID_TO_GRID_LAYOUT = "$toGridLayout";
const QString ID_TO_STACKED_LAYOUT = "$toStackedLayout";
const QString ID_TO_TAB_WIDGET = "$toTabWidget";
const QString ID_TO_SPLITTER = "$toSplitter";

const QString ID_SET_PROPERTY = "$setProperty";
const QString ID_RESIZE = "$resize";
const QString ID_MAXIMIZE = "$maximize";

const QString ID_ID = "id";
const QString ID_FORM_ID = "form_id";
const QString ID_LAYOUT_ID = "layout_id";
const QString ID_TAB_WIDGET_ID = "tab_widget_id";
const QString ID_SPLITTER_ID = "splitter_id";
const QString ID_PARENT_ID = "parent_id";
const QString ID_MENU_ID = "menu_id";
const QString ID_WIDGET_ID = "widget_id";
const QString ID_LABEL_ID = "label_id";
const QString ID_FIELD_ID = "field_id";
const QString ID_TITLE = "title";
const QString ID_TEXT = "text";
const QString ID_ITEMS = "items";
const QString ID_NAME = "name";
const QString ID_TYPE = "type";
const QString ID_VALUE = "value";
const QString ID_ROW = "row";
const QString ID_COLUMN = "column";
const QString ID_WIDTH = "width";
const QString ID_HEIGHT = "height";
const QString ID_LABEL = "label";
const QString ID_ORIENTATION = "orientation";

const QString ID_HORIZONTAL = "horizontal";
const QString ID_VERTICAL = "vertical";


#endif // IDS_H
