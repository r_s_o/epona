#ifndef UTILS_H
#define UTILS_H

#include <QString>

#include <iostream>
using namespace std;

inline std::ostream& operator<<(std::ostream& stream, const QString& str)
{
   stream<<str.toStdString();
   return stream;
}

#endif // UTILS_H
