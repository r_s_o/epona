#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include <QObject>
//#include <QPoint>

class EventHandler: public QObject
{
   Q_OBJECT

public:
   explicit EventHandler(QObject* parent=nullptr) : QObject(parent) {}

protected:
   bool eventFilter(QObject* obj, QEvent* event);

signals:
   void rightPressed(QString id);
};

#endif // EVENTHANDLER_H
