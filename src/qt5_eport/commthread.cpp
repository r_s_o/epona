#include "commthread.h"
#include "ids.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QVariantList>

CommThread::CommThread()
{
}

void CommThread::run()
{
   // Helper function to call actual widgets' creating and such.
   // Could be simplified further, but there is no real need now.
   auto wrapper = [this](Json json, QString command, Response fun)
   {
      try
      {
         fun(json);
      }
      catch(json_exception)
      {
         cerr<<command.toStdString()<<": bad json: "<<json<<endl<<flush;
      }
   };

   // These are just forwarders, so for the sake of brewity keeping it right here.
   static const Responses responses=
   {
      { ID_CLOSE, [this](Json){ emit e_close(); return ""; } },
      {
         ID_INFORMATION_BOX, [this, wrapper](Json json)
         {
            return wrapper(json, ID_INFORMATION_BOX, [this](Json json)
            {
               auto[title, text] = extract(json, ID_TITLE, ID_TEXT);
               emit e_information(title.toString(), text.toString());
            });
         }
      },
      {
         ID_WARNING_BOX, [this, wrapper](Json json)
         {
            return wrapper(json, ID_WARNING_BOX, [this](Json json)
            {
               auto[title, text] = extract(json, ID_TITLE, ID_TEXT);
               emit e_warning(title.toString(), text.toString());
            });
         }
      },
      {
         ID_QUESTION_BOX, [this, wrapper](Json json)
         {
            return wrapper(json, ID_QUESTION_BOX, [this](Json json)
            {
               auto[id, title, text] = extract(json, ID_ID, ID_TITLE, ID_TEXT);
               emit e_question(id.toString(), title.toString(), text.toString());
            });
         }
      },
      {
         ID_FORM, [this, wrapper](Json json)
         {
            return wrapper(json, ID_FORM, [this](Json json)
            {
               auto[id, title] = extract(json, ID_ID, ID_TITLE);
               emit e_form(id.toString(), title.toString());
            });
         }
      },
      {
         ID_HORIZONTAL_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_HORIZONTAL_LAYOUT, [this](Json json)
            {
               auto[formId, id, parentId] = extract(json, ID_FORM_ID, ID_ID, ID_PARENT_ID);
               emit e_horizontalLayout(formId.toString(), id.toString(), parentId.toString());
            });
         }
      },
      {
         ID_VERTICAL_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_VERTICAL_LAYOUT, [this](Json json)
            {
               auto[formId, id, parentId] = extract(json, ID_FORM_ID, ID_ID, ID_PARENT_ID);
               emit e_verticalLayout(formId.toString(), id.toString(), parentId.toString());
            });
         }
      },
      {
         ID_FORM_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_FORM_LAYOUT, [this](Json json)
            {
               auto[formId, id, parentId] = extract(json, ID_FORM_ID, ID_ID, ID_PARENT_ID);
               emit e_formLayout(formId.toString(), id.toString(), parentId.toString());
            });
         }
      },
      {
         ID_GRID_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_GRID_LAYOUT, [this](Json json)
            {
               auto[formId, id, parentId] = extract(json, ID_FORM_ID, ID_ID, ID_PARENT_ID);
               emit e_gridLayout(formId.toString(), id.toString(), parentId.toString());
            });
         }
      },
      {
         ID_STACKED_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_STACKED_LAYOUT, [this](Json json)
            {
               auto[formId, id, parentId] = extract(json, ID_FORM_ID, ID_ID, ID_PARENT_ID);
               emit e_stackedLayout(formId.toString(), id.toString(), parentId.toString());
            });
         }
      },
      {
         ID_STRETCHER, [this, wrapper](Json json)
         {
            return wrapper(json, ID_STRETCHER, [this](Json json)
            {
               auto[layoutId] = extract(json, ID_LAYOUT_ID);
               emit e_stretcher(layoutId.toString());
            });
         }
      },

      {
         ID_CHECK_BOX, [this, wrapper](Json json)
         {
            return wrapper(json, ID_CHECK_BOX, [this](Json json)
            {
               auto[id, text] = extract(json, ID_ID, ID_TEXT);
               emit e_checkBox(id.toString(), text.toString());
            });
         }
      },
      {
         ID_COMBO_BOX, [this, wrapper](Json json)
         {
            return wrapper(json, ID_COMBO_BOX, [this](Json json)
            {
               auto[id, items] = extract(json, ID_ID, ID_ITEMS);
               QStringList itemStrings;
               for(auto i: items.toArray().toVariantList())
                  itemStrings.append(i.toString());
               emit e_comboBox(id.toString(), itemStrings);
            });
         }
      },
      {
         ID_GROUP_BOX, [this, wrapper](Json json)
         {
            return wrapper(json, ID_PUSH_BUTTON, [this](Json json)
            {
               auto[id, title] = extract(json, ID_ID, ID_TITLE);
               emit e_groupBox(id.toString(), title.toString());
            });
         }
      },
      {
         ID_CREATE_LABEL, [this, wrapper](Json json)
         {
            return wrapper(json, ID_CREATE_LABEL, [this](Json json)
            {
               auto[id, text] = extract(json, ID_ID, ID_TEXT);
               emit e_label(id.toString(), text.toString());
            });
         }
      },
      {
         ID_LINE_EDIT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_LINE_EDIT, [this](Json json)
            {
               auto[id, text] = extract(json, ID_ID, ID_TEXT);
               emit e_lineEdit(id.toString(), text.toString());
            });
         }
      },
      {
         ID_LIST_WIDGET, [this, wrapper](Json json)
         {
            return wrapper(json, ID_LIST_WIDGET, [this](Json json)
            {
               auto[id, items] = extract(json, ID_ID, ID_ITEMS);
               QStringList itemStrings;
               for(auto i: items.toArray().toVariantList())
                  itemStrings.append(i.toString());
               emit e_listWidget(id.toString(), itemStrings);
            });
         }
      },
      {
         ID_PUSH_BUTTON, [this, wrapper](Json json)
         {
            return wrapper(json, ID_PUSH_BUTTON, [this](Json json)
            {
               auto[id, text] = extract(json, ID_ID, ID_TEXT);
               emit e_pushButton(id.toString(), text.toString());
            });
         }
      },
      {
         ID_RADIO_BUTTON, [this, wrapper](Json json)
         {
            return wrapper(json, ID_RADIO_BUTTON, [this](Json json)
            {
               auto[id, text] = extract(json, ID_ID, ID_TEXT);
               emit e_radioButton(id.toString(), text.toString());
            });
         }
      },
      {
         ID_SPLITTER, [this, wrapper](Json json)
         {
            return wrapper(json, ID_SPLITTER, [this](Json json)
            {
               auto[id, orientation] = extract(json, ID_ID, ID_ORIENTATION);
               emit e_splitter(id.toString(), orientation.toString());
            });
         }
      },
      {
         ID_TAB_WIDGET, [this, wrapper](Json json)
         {
            return wrapper(json, ID_TAB_WIDGET, [this](Json json)
            {
               auto[id] = extract(json, ID_ID);
               emit e_tabWidget(id.toString());
            });
         }
      },
      {
         ID_WIDGET, [this, wrapper](Json json)
         {
            return wrapper(json, ID_WIDGET, [this](Json json)
            {
               auto[id] = extract(json, ID_ID);
               emit e_widget(id.toString());
            });
         }
      },

      {
         ID_MENU, [this, wrapper](Json json)
         {
            return wrapper(json, ID_MENU, [this](Json json)
            {
               auto[id] = extract(json, ID_ID);
               emit e_menu(id.toString());
            });
         }
      },
      {
         ID_SUBMENU, [this, wrapper](Json json)
         {
            return wrapper(json, ID_SUBMENU, [this](Json json)
            {
               auto[id, text, parentId] = extract(json, ID_ID, ID_TEXT, ID_PARENT_ID);
               emit e_submenu(id.toString(), text.toString(), parentId.toString());
            });
         }
      },
      {
         ID_MENU_ITEM, [this, wrapper](Json json)
         {
            return wrapper(json, ID_MENU_ITEM, [this](Json json)
            {
               auto[id, text, menuId] = extract(json, ID_ID, ID_TEXT, ID_MENU_ID);
               emit e_menuItem(id.toString(), text.toString(), menuId.toString());
            });
         }
      },
      {
         ID_MENU_SEPARATOR, [this, wrapper](Json json)
         {
            return wrapper(json, ID_MENU_SEPARATOR, [this](Json json)
            {
               auto[id, menuId] = extract(json, ID_ID, ID_MENU_ID);
               emit e_menuSeparator(id.toString(), menuId.toString());
            });
         }
      },
      {
         ID_MENU_BAR, [this, wrapper](Json json)
         {
            return wrapper(json, ID_MENU_BAR, [this](Json json)
            {
               auto[id] = extract(json, ID_ID);
               emit e_menuBar(id.toString());
            });
         }
      },
      {
         ID_POPUP_MENU, [this, wrapper](Json json)
         {
            return wrapper(json, ID_POPUP_MENU, [this](Json json)
            {
               auto[menuId, formId, widgetId] = extract(json, ID_MENU_ID, ID_FORM_ID, ID_WIDGET_ID);
               emit e_popupMenu(menuId.toString(), formId.toString(), widgetId.toString());
            });
         }
      },

      {
         ID_TO_BOX_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_TO_BOX_LAYOUT, [this](Json json)
            {
               auto[formId, id, layoutId] = extract(json, ID_FORM_ID, ID_ID, ID_LAYOUT_ID);
               emit e_toBoxLayout(formId.toString(), id.toString(), layoutId.toString());
            });
         }
      },
      {
         ID_TO_FORM_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_TO_FORM_LAYOUT, [this](Json json)
            {
               auto[formId, labelId, fieldId, layoutId] = extract(json, ID_FORM_ID, ID_LABEL_ID, ID_FIELD_ID, ID_LAYOUT_ID);
               emit e_toFormLayout(formId.toString(), labelId.toString(), fieldId.toString(), layoutId.toString());
            });
         }
      },
      {
         ID_TO_GRID_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_TO_GRID_LAYOUT, [this](Json json)
            {
               auto[formId, id, layoutId, row, column] = extract(json, ID_FORM_ID, ID_ID, ID_LAYOUT_ID, ID_ROW, ID_COLUMN);
               emit e_toGridLayout(formId.toString(), id.toString(), layoutId.toString(), row.toInt(), column.toInt());
            });
         }
      },
      {
         ID_TO_STACKED_LAYOUT, [this, wrapper](Json json)
         {
            return wrapper(json, ID_TO_STACKED_LAYOUT, [this](Json json)
            {
               auto[formId, id, layoutId] = extract(json, ID_FORM_ID, ID_ID, ID_LAYOUT_ID);
               emit e_toStackedLayout(formId.toString(), id.toString(), layoutId.toString());
            });
         }
      },
      {
         ID_TO_TAB_WIDGET, [this, wrapper](Json json)
         {
            return wrapper(json, ID_TO_TAB_WIDGET, [this](Json json)
            {
               auto[formId, id, tabWidgetId, label] = extract(json, ID_FORM_ID, ID_ID, ID_TAB_WIDGET_ID, ID_LABEL);
               emit e_toTabWidget(formId.toString(), id.toString(), tabWidgetId.toString(), label.toString());
            });
         }
      },
      {
         ID_TO_SPLITTER, [this, wrapper](Json json)
         {
            return wrapper(json, ID_TO_SPLITTER, [this](Json json)
            {
               auto[formId, id, splitterId] = extract(json, ID_FORM_ID, ID_ID, ID_SPLITTER_ID);
               emit e_toSplitter(formId.toString(), id.toString(), splitterId.toString());
            });
         }
      },

      {
         ID_SET_PROPERTY, [this, wrapper](Json json)
         {
            return wrapper(json, ID_SET_PROPERTY, [this](Json json)
            {
               auto[formId, id, name, value] = extract(json, ID_FORM_ID, ID_ID, ID_NAME, ID_VALUE);
               emit e_setProperty(formId.toString(), id.toString(), name.toString(), value.toString());
            });
         }
      },
      {
         ID_RESIZE, [this, wrapper](Json json)
         {
            return wrapper(json, ID_RESIZE, [this](Json json)
            {
               auto[formId, width, height] = extract(json, ID_FORM_ID, ID_WIDTH, ID_HEIGHT);
               emit e_resize(formId.toString(), width.toInt(), height.toInt());
            });
         }
      },
      {
         ID_MAXIMIZE, [this, wrapper](Json json)
         {
            return wrapper(json, ID_MAXIMIZE, [this](Json json)
            {
               auto[formId] = extract(json, ID_FORM_ID);
               emit e_maximize(formId.toString());
            });
         }
      }
   };

   // Our main loop. Reading input from stdin, parsing (kinda), processing.
   for(;;)
   {
      string s;
      getline(cin, s);

      // Splitting s; first lexeme is the command, rest is json.
      auto[first, rest] = split(QString::fromStdString(s));
      //      cerr<<"first: '"<<first.toStdString()<<"'"<<endl;
      //      cerr<<"rest: '"<<rest.toStdString()<<"'"<<endl;

      // Special case.
      if(ID_EXIT==first)
      {
         emit e_close(); // Forced closing of the main window.
         break;
      }

      // All the other responses.
      auto r=response(responses, first);
      if(r) // There is a proper response.
      {
         (*r)(rest); // Execute it.
      }
      else // Nothing meaninful found.
      {
         continue;
      }
   }
}


// Utilities

tuple<QString, QString> CommThread::split(QString s)
{
   int firstSpace = s.indexOf(" ");
   if(-1==firstSpace)
      return {s, ""};
   return {s.left(firstSpace), s.right(s.length()-firstSpace)};
}

QJsonDocument CommThread::convert(Json json)
{
   QJsonDocument d = QJsonDocument::fromJson(json.toUtf8());
   if(!d.isObject())
      throw json_exception();
   return d;
}

tuple<QJsonValue>
      CommThread::extract(Json json, QString id1)
{
   QJsonDocument d = convert(json);
   QJsonValue v1 = d[id1];
   if(v1.isUndefined())
      throw json_exception();
   return make_tuple(v1);
}

tuple<QJsonValue, QJsonValue>
      CommThread::extract(Json json, QString id1, QString id2)
{
   QJsonDocument d = convert(json);
   QJsonValue v1 = d[id1];
   QJsonValue v2 = d[id2];
   if(v1.isUndefined() || v2.isUndefined())
      throw json_exception();
   return make_tuple(v1, v2);
}

tuple<QJsonValue, QJsonValue, QJsonValue>
      CommThread::extract(Json json, QString id1, QString id2, QString id3)
{
   QJsonDocument d = convert(json);
   QJsonValue v1 = d[id1];
   QJsonValue v2 = d[id2];
   QJsonValue v3 = d[id3];
   if(v1.isUndefined() || v2.isUndefined() || v3.isUndefined())
      throw json_exception();
   return make_tuple(v1, v2, v3);
}

tuple<QJsonValue, QJsonValue, QJsonValue, QJsonValue>
      CommThread::extract(Json json, QString id1, QString id2, QString id3, QString id4)
{
   QJsonDocument d = convert(json);
   QJsonValue v1 = d[id1];
   QJsonValue v2 = d[id2];
   QJsonValue v3 = d[id3];
   QJsonValue v4 = d[id4];
   if(v1.isUndefined() || v2.isUndefined() || v3.isUndefined() || v4.isUndefined())
      throw json_exception();
   return make_tuple(v1, v2, v3, v4);
}

tuple<QJsonValue, QJsonValue, QJsonValue, QJsonValue, QJsonValue>
      CommThread::extract(Json json, QString id1, QString id2, QString id3, QString id4, QString id5)
{
   QJsonDocument d = convert(json);
   QJsonValue v1 = d[id1];
   QJsonValue v2 = d[id2];
   QJsonValue v3 = d[id3];
   QJsonValue v4 = d[id4];
   QJsonValue v5 = d[id5];
   if(v1.isUndefined() || v2.isUndefined() || v3.isUndefined() || v4.isUndefined() || v5.isUndefined())
      throw json_exception();
   return make_tuple(v1, v2, v3, v4, v5);
}

