qt5_eport
=========


What
----

A so called "port" (not a "port driver"!) of the Qt5 framework library. It is intended to be used with (called from) an Erlang app to build and to control a Qt-based graphical user interface.


Why
---

Someone has to.


How
---

See Erlang source files to get the general idea.

(Actually, the binary itself communicates with it's Erlang partner over stdin, so it can be controlled from a terminal as well. Not really feasible though.)


Build
-----

C++17 capable compiler. Gcc's preferable; clang's not tested. Cmake. Qt5 obviously.

    cd build
    ../src/qt5_eport/ && cmake --build .
    cd ..
    
or just
    
    ./p.sh


Run
---
    rebar3 shell --apps epona

or just

    ./r.sh


Who
---

r.s.o.aliev@gmail.com

