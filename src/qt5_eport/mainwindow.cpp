#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   setWindowIcon(QIcon(":/svg/erlang.svg"));
}

MainWindow::~MainWindow()
{
   delete ui;
}

void MainWindow::changeEvent(QEvent* e)
{
   QMainWindow::changeEvent(e);
   switch (e->type()) {
   case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
   default:
      break;
   }
}


// Slots

void MainWindow::e_stretcher(QString layoutId)
{
   //TODO FormId
   auto parentLayout = this->findChild<QBoxLayout*>(layoutId);
   if(!parentLayout)
      parentLayout = qobject_cast<QBoxLayout*>(centralWidget()->layout());
   if(!parentLayout)
   {
      cerr<<"e_stretcher: couldn't find "<<layoutId.toStdString()<<endl<<flush;
      return;
   }
   parentLayout->addStretch();
}


void MainWindow::e_toBoxLayout(QString formId, QString id, QString layoutId)
{
   //TODO This one's pretty fragile. Think of ways to make it robust.

   // Try and find the form named @formId.
   auto form = findFormById(formId);
   // Try and find the layout named @layoutId.
   auto parentLayout = form->findChild<QBoxLayout*>(layoutId);
   if(!parentLayout) // Not found. Lets try to interpret centralWidget's one.
      parentLayout = qobject_cast<QBoxLayout*>(centralWidget()->layout());
   if(!parentLayout) // Still no luck.
   {
      cerr<<"e_toBoxLayout: couldn't find "<<layoutId.toStdString()<<", and centralWidget's layout is not a BoxLayout"<<endl<<flush;
      return;
   }
   // Try and find the widget named @id.
   //TODO Always looking in the MainWindow; is it feasible?
   if( auto widget = this->findChild<QWidget*>(id) )
   {
      parentLayout->addWidget(widget);
      return;
   }

   // Total fiasco.
   cerr<<"e_toBoxLayout: no such layout or widget: "<<layoutId.toStdString()<<endl<<flush;
}

void MainWindow::e_toFormLayout(QString formId, QString labelId, QString fieldId, QString layoutId)
{
   auto form = findFormById(formId);
   auto parentLayout = form->findChild<QFormLayout*>(layoutId);
   if(!parentLayout)
      parentLayout = qobject_cast<QFormLayout*>(centralWidget()->layout());
   if(!parentLayout)
   {
      cerr<<"e_toFormLayout: couldn't find "<<layoutId.toStdString()<<", and centralWidget's layout is not a FormLayout"<<endl<<flush;
      return;
   }
   auto labelWidget = this->findChild<QWidget*>(labelId);
   auto fieldWidget = this->findChild<QWidget*>(fieldId);
   if(labelWidget && fieldWidget)
   {
      parentLayout->addRow(labelWidget, fieldWidget);
      return;
   }

   cerr<<"e_toFormLayout: no such layout or widgets: {"<<labelId.toStdString()<<", "<<fieldId.toStdString()<<"}"<<endl<<flush;
}

void MainWindow::e_toGridLayout(QString formId, QString id, QString layoutId, int row, int column)
{
   auto form = findFormById(formId);
   auto parentLayout = form->findChild<QGridLayout*>(layoutId);
   if(!parentLayout)
      parentLayout = qobject_cast<QGridLayout*>(centralWidget()->layout());
   if(!parentLayout)
   {
      cerr<<"e_toGridLayout: couldn't find "<<layoutId.toStdString()<<", and centralWidget's layout is not a GridLayout"<<endl<<flush;
      return;
   }
   if( auto widget = this->findChild<QWidget*>(id) )
   {
      parentLayout->addWidget(widget, row, column);
      return;
   }

   cerr<<"e_toGridLayout: no such layout or widget"<<id.toStdString()<<endl<<flush;
}

void MainWindow::e_toStackedLayout(QString formId, QString id, QString layoutId)
{
   auto form = findFormById(formId);
   auto parentLayout = form->findChild<QStackedLayout*>(layoutId);
   if(!parentLayout)
      parentLayout = qobject_cast<QStackedLayout*>(centralWidget()->layout());
   if(!parentLayout)
   {
      cerr<<"e_toStackedLayout: couldn't find "<<layoutId.toStdString()<<", and centralWidget's layout is not a StackedLayout"<<endl<<flush;
      return;
   }
   if( auto widget = this->findChild<QWidget*>(id) )
   {
      parentLayout->addWidget(widget);
      return;
   }

   cerr<<"e_toStackedLayout: no such layout or widget: "<<layoutId.toStdString()<<endl<<flush;
}

void MainWindow::e_toTabWidget(QString formId, QString id, QString tabWidgetId, QString label)
{
   auto form = findFormById(formId);
   auto tabWidget = form->findChild<QTabWidget*>(tabWidgetId);
   if(!tabWidget)
   {
      cerr<<"e_toTabWidget: couldn't find "<<tabWidgetId<<endl<<flush;
      return;
   }
   if( auto widget = this->findChild<QWidget*>(id) )
   {
      tabWidget->addTab(widget, label);
      return;
   }

   cerr<<"e_toTabWidget: no such widget: "<<id<<endl<<flush;
}

void MainWindow::e_toSplitter(QString formId, QString id, QString splitterId)
{
   if( auto form = findFormById(formId) )
   {
      if( auto splitter = form->findChild<QSplitter*>(splitterId) )
      {
         if( auto widget = this->findChild<QWidget*>(id) )
         {
            splitter->addWidget(widget);
            return;
         }
      }
   }
   cerr<<"e_toSplitter: no such splitter: "<<id<<", "<<splitterId<<endl<<flush;
}


// Utilities

QWidget* MainWindow::findFormById(QString formId)
{
   if(""==formId || "MainWindow"==formId)
      return this;
   QDialog* d=findChild<QDialog*>(formId);
   return d ? qobject_cast<QWidget*>(d) : qobject_cast<QWidget*>(this);
}

QObject* MainWindow::findById(QWidget* form, QString id)
{
   // Special cases.
   if("MainWindow"==id)
      return this;
   if(""==id)
      return this->centralWidget();
   // Try to find a layout first.
   QLayout* layout=form->findChild<QLayout*>(id);
   if(layout)
      return layout;
   // No such layout, so it must be a widget.
   return form->findChild<QWidget*>(id);
}

bool MainWindow::setParentById(QLayout* l, QString formId, QString parentId)
{
   QWidget* form=findFormById(formId);
   QObject* parent=findById(form, parentId);
   if(qobject_cast<QWidget*>(parent))
      qobject_cast<QWidget*>(parent)->setLayout(l);
   else if(qobject_cast<QBoxLayout*>(parent))
      qobject_cast<QBoxLayout*>(parent)->addLayout(l);
   else
   {
      if(form==this)
         this->centralWidget()->setLayout(l);
      else
         form->setLayout(l);
   }
   return true;
}

