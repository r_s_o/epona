#include "mainwindow.h"
#include <QApplication>
#include "commthread.h"

#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
   QApplication a(argc, argv);
   MainWindow w;
   w.show();

   CommThread* t = new CommThread();

   // Client -> Server
   QObject::connect(t, &CommThread::e_close, &w, &MainWindow::e_close);

   QObject::connect(t, &CommThread::e_form, &w, &MainWindow::e_form);

   QObject::connect(t, &CommThread::e_information, &w, &MainWindow::e_information);
   QObject::connect(t, &CommThread::e_warning, &w, &MainWindow::e_warning);
   QObject::connect(t, &CommThread::e_question, &w, &MainWindow::e_question);

   QObject::connect(t, &CommThread::e_horizontalLayout, &w, &MainWindow::e_horizontalLayout);
   QObject::connect(t, &CommThread::e_verticalLayout, &w, &MainWindow::e_verticalLayout);
   QObject::connect(t, &CommThread::e_formLayout, &w, &MainWindow::e_formLayout);
   QObject::connect(t, &CommThread::e_gridLayout, &w, &MainWindow::e_gridLayout);
   QObject::connect(t, &CommThread::e_stackedLayout, &w, &MainWindow::e_stackedLayout);
   QObject::connect(t, &CommThread::e_stretcher, &w, &MainWindow::e_stretcher);

   QObject::connect(t, &CommThread::e_checkBox, &w, &MainWindow::e_checkBox);
   QObject::connect(t, &CommThread::e_comboBox, &w, &MainWindow::e_comboBox);
   QObject::connect(t, &CommThread::e_groupBox, &w, &MainWindow::e_groupBox);
   QObject::connect(t, &CommThread::e_label, &w, &MainWindow::e_label);
   QObject::connect(t, &CommThread::e_lineEdit, &w, &MainWindow::e_lineEdit);
   QObject::connect(t, &CommThread::e_listWidget, &w, &MainWindow::e_listWidget);
   QObject::connect(t, &CommThread::e_pushButton, &w, &MainWindow::e_pushButton);
   QObject::connect(t, &CommThread::e_radioButton, &w, &MainWindow::e_radioButton);
   QObject::connect(t, &CommThread::e_splitter, &w, &MainWindow::e_splitter);
   QObject::connect(t, &CommThread::e_tabWidget, &w, &MainWindow::e_tabWidget);
   QObject::connect(t, &CommThread::e_widget, &w, &MainWindow::e_widget);

   QObject::connect(t, &CommThread::e_menu, &w, &MainWindow::e_menu);
   QObject::connect(t, &CommThread::e_submenu, &w, &MainWindow::e_submenu);
   QObject::connect(t, &CommThread::e_menuItem, &w, &MainWindow::e_menuItem);
   QObject::connect(t, &CommThread::e_menuSeparator, &w, &MainWindow::e_menuSeparator);
   QObject::connect(t, &CommThread::e_menuBar, &w, &MainWindow::e_menuBar);
   QObject::connect(t, &CommThread::e_popupMenu, &w, &MainWindow::e_popupMenu);

   QObject::connect(t, &CommThread::e_toBoxLayout, &w, &MainWindow::e_toBoxLayout);
   QObject::connect(t, &CommThread::e_toFormLayout, &w, &MainWindow::e_toFormLayout);
   QObject::connect(t, &CommThread::e_toGridLayout, &w, &MainWindow::e_toGridLayout);
   QObject::connect(t, &CommThread::e_toStackedLayout, &w, &MainWindow::e_toStackedLayout);
   QObject::connect(t, &CommThread::e_toTabWidget, &w, &MainWindow::e_toTabWidget);
   QObject::connect(t, &CommThread::e_toSplitter, &w, &MainWindow::e_toSplitter);

   QObject::connect(t, &CommThread::e_setProperty, &w, &MainWindow::e_setProperty);
   QObject::connect(t, &CommThread::e_resize, &w, &MainWindow::e_resize);
   QObject::connect(t, &CommThread::e_maximize, &w, &MainWindow::e_maximize);

   // Server -> Client
   QObject::connect(&w, &MainWindow::e_questionResponse, t, &CommThread::e_questionResponse);

   QObject::connect(&w, &MainWindow::e_buttonClicked, t, &CommThread::e_buttonClicked);
   QObject::connect(&w, &MainWindow::e_buttonPressed, t, &CommThread::e_buttonPressed);
   QObject::connect(&w, &MainWindow::e_buttonReleased, t, &CommThread::e_buttonReleased);
   QObject::connect(&w, &MainWindow::e_buttonToggled, t, &CommThread::e_buttonToggled);
   QObject::connect(&w, &MainWindow::e_buttonRightPressed, t, &CommThread::e_buttonRightPressed);

   QObject::connect(&w, &MainWindow::e_checkBoxStateChanged, t, &CommThread::e_checkBoxStateChanged);

   QObject::connect(&w, &MainWindow::e_comboBoxCurrentIndexChanged, t, &CommThread::e_comboBoxCurrentIndexChanged);
   QObject::connect(&w, &MainWindow::e_comboBoxCurrentTextChanged, t, &CommThread::e_comboBoxCurrentTextChanged);
   QObject::connect(&w, &MainWindow::e_comboBoxEditTextChanged, t, &CommThread::e_comboBoxEditTextChanged);

   QObject::connect(&w, &MainWindow::e_lineEditTextEdited, t, &CommThread::e_lineEditTextEdited);

   QObject::connect(&w, &MainWindow::e_tabWidgetCurrentChanged, t, &CommThread::e_tabWidgetCurrentChanged);
   QObject::connect(&w, &MainWindow::e_tabWidgetTabBarClicked, t, &CommThread::e_tabWidgetTabBarClicked);
   QObject::connect(&w, &MainWindow::e_tabWidgetTabBarDoubleClicked, t, &CommThread::e_tabWidgetTabBarDoubleClicked);
   QObject::connect(&w, &MainWindow::e_tabWidgetTabCloseRequested, t, &CommThread::e_tabWidgetTabCloseRequested);

   QObject::connect(&w, &MainWindow::e_menuTriggered, t, &CommThread::e_menuTriggered);

   t->start();

   return a.exec();
}
