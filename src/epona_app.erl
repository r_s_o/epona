-module(epona_app).
-author("ra").
-behaviour(application).
-export([start/2, stop/1]).

%% Callbacks

start(_StartType, _StartArgs) ->
	case epona_sup:start_link() of
		{ok, Pid} ->
			{ok, Pid};
		Error ->
			Error
	end.

stop(_State) -> ok.

