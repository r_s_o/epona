-module(epona_sup).
-author("ra").
-behaviour(supervisor).
-export([start_link/0]).
-export([init/1]).


%% API

start_link() ->
	io:format("epona_sup:start_link~n"),
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).


%% Callbacks

init([]) ->
	MaxRestarts = 0, %1000,
	MaxSecondsBetweenRestarts = 3600,
	Flags =
		#{
			strategy => one_for_one,
			intensity => MaxRestarts,
			period => MaxSecondsBetweenRestarts
		},
	Epona =
		#{
			id => epona,
			start => {epona, start_link, []},
			restart => permanent,
			shutdown => 2000,
			type => worker
		},
%%	Demo =
%%		#{
%%			id => demo_gs,
%%			start => {demo_gs, start_link, []},
%%			restart => permanent,
%%			shutdown => 2000,
%%			type => worker
%%		},
%%	{ok, {Flags, [Epona, Demo]}}.
	Minesweeper =
		#{
			id => minesweeper,
			start => {minesweeper, start_link, []},
			restart => permanent,
			shutdown => 2000,
			type => worker
		},
	{ok, {Flags, [Epona, Minesweeper]}}.

