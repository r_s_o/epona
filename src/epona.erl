-module(epona).
-author("ra").
-behaviour(gen_server).
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2]).
-export([form/2]).
-export([informationDialog/2, warningDialog/2, questionDialog/4]).
-export([
	horizontalLayout/3, horizontalLayout/2,
	verticalLayout/3, verticalLayout/2,
	formLayout/3, formLayout/2,
	gridLayout/3, gridLayout/2,
	stackedLayout/3, stackedLayout/2,
	stretcher/1]).
-export([
	checkBox/2,
	comboBox/2,
	groupBox/3, groupBox/2,
	label/2,
	lineEdit/2,
	listWidget/2,
	pushButton/2,
	radioButton/2,
	splitter/2,
	tabWidget/1,
	widget/2, widget/1]).
-export([
	menu/1,
	submenu/3,
	menuItem/3,
	menuSeparator/2,
	menuBar/1,
	popupMenu/3, popupMenu/2]).
-export([
	toBoxLayout/3, toBoxLayout/2,
	toFormLayout/4, toFormLayout/3,
	toGridLayout/5, toGridLayout/4,
	toStackedLayout/3, toStackedLayout/2,
	toTabWidget/4, toTabWidget/3,
	toSplitter/3, toSplitter/2]).
-export([
	property/3,	property/2,
	properties/3, properties/2,
	resize/3, resize/2, maximize/1, close/0]).
-export([subscribe/2]).
-export([batch/1, newId/0, idToLayoutId/1]).
-export_type([id/0]).
-include("ids.hrl").

-type id() :: binary().

%%
-record(state,
{
	qt_port,
	clients = #{},
	subscribers = #{}
}).


%% API

start_link() ->
	io:format("epona:start_link~n"),
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec form(id(), binary()) -> ok | {error, Reason :: string()}.
form(Id, Title) -> gen_server:call(?MODULE, {form, Id, Title}).

-spec informationDialog(string(), string()) -> ok | {error, Reason :: string()}.
informationDialog(Title, Text) -> gen_server:call(?MODULE, {information_dialog, Title, Text}).
-spec warningDialog(string(), string()) -> ok | {error, Reason :: string()}.
warningDialog(Title, Text) -> gen_server:call(?MODULE, {warning_dialog, Title, Text}).
-spec questionDialog(pid(), binary(), string(), string()) -> ok | {error, Reason :: string()}.
questionDialog(ClientPid, RequestId, Title, Text) ->
	gen_server:call(?MODULE, {question_dialog, ClientPid, RequestId, Title, Text}).

-spec horizontalLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
horizontalLayout(FormId, Id, ParentId) -> gen_server:call(?MODULE, {horizontal_layout, FormId, Id, ParentId}).
-spec horizontalLayout(binary(), binary()) -> ok | {error, Reason :: string()}.
horizontalLayout(Id, ParentId) -> horizontalLayout(?EPONA_MAIN_WINDOW, Id, ParentId).
-spec verticalLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
verticalLayout(FormId, Id, ParentId) -> gen_server:call(?MODULE, {vertical_layout, FormId, Id, ParentId}).
-spec verticalLayout(binary(), binary()) -> ok | {error, Reason :: string()}.
verticalLayout(Id, ParentId) -> verticalLayout(?EPONA_MAIN_WINDOW, Id, ParentId).
-spec formLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
formLayout(FormId, Id, ParentId) -> gen_server:call(?MODULE, {form_layout, FormId, Id, ParentId}).
-spec formLayout(binary(), binary()) -> ok | {error, Reason :: string()}.
formLayout(Id, ParentId) -> formLayout(?EPONA_MAIN_WINDOW, Id, ParentId).
-spec gridLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
gridLayout(FormId, Id, ParentId) -> gen_server:call(?MODULE, {grid_layout, FormId, Id, ParentId}).
-spec gridLayout(binary(), binary()) -> ok | {error, Reason :: string()}.
gridLayout(Id, ParentId) -> gridLayout(?EPONA_MAIN_WINDOW, Id, ParentId).
-spec stackedLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
stackedLayout(FormId, Id, ParentId) -> gen_server:call(?MODULE, {stacked_layout, FormId, Id, ParentId}).
-spec stackedLayout(binary(), binary()) -> ok | {error, Reason :: string()}.
stackedLayout(Id, ParentId) -> stackedLayout(?EPONA_MAIN_WINDOW, Id, ParentId).

stretcher(LayoutId) -> gen_server:call(?MODULE, {stretcher, LayoutId}).

-spec checkBox(binary(), binary()) -> ok | {error, Reason :: string()}.
checkBox(Id, Text) -> gen_server:call(?MODULE, {check_box, Id, Text}).
-spec comboBox(binary(), [string()] | [binary()]) -> ok | {error, Reason :: string()}.
comboBox(Id, Items) -> gen_server:call(?MODULE, {combo_box, Id, Items}).
-spec groupBox(binary(), binary(), atom()) -> ok | {error, Reason :: string()}.
groupBox(Id, Title, LayoutType) -> gen_server:call(?MODULE, {group_box, Id, Title, LayoutType}).
-spec groupBox(binary(), binary()) -> ok | {error, Reason :: string()}.
groupBox(Id, Title) -> groupBox(Id, Title, no_layout).
-spec label(binary(), binary()) -> ok | {error, Reason :: string()}.
label(Id, Text) -> gen_server:call(?MODULE, {label, Id, Text}).
-spec lineEdit(binary(), binary()) -> ok | {error, Reason :: string()}.
lineEdit(Id, Text) -> gen_server:call(?MODULE, {line_edit, Id, Text}).
-spec listWidget(binary(), [string()] | [binary()]) -> ok | {error, Reason :: string()}.
listWidget(Id, Items) -> gen_server:call(?MODULE, {list_widget, Id, Items}).
-spec pushButton(binary(), binary()) -> ok | {error, Reason :: string()}.
pushButton(Id, Text) -> gen_server:call(?MODULE, {push_button, Id, Text}).
-spec radioButton(binary(), binary()) -> ok | {error, Reason :: string()}.
radioButton(Id, Text) -> gen_server:call(?MODULE, {radio_button, Id, Text}).
-spec splitter(binary(), horizontal | vertical) -> ok | {error, Reason :: string()}.
splitter(Id, Orientation) -> gen_server:call(?MODULE, {splitter, Id, Orientation}).
-spec tabWidget(binary()) -> ok | {error, Reason :: string()}.
tabWidget(Id) -> gen_server:call(?MODULE, {tab_widget, Id}).
-spec widget(binary(), atom()) -> ok | {error, Reason :: string()}.
widget(Id, LayoutType) -> gen_server:call(?MODULE, {widget, Id, LayoutType}).
-spec widget(binary()) -> ok | {error, Reason :: string()}.
widget(Id) -> widget(Id, no_layout).

-spec menu(binary()) -> ok | {error, Reason :: string()}.
menu(Id) -> gen_server:call(?MODULE, {menu, Id}).
-spec submenu(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
submenu(Id, Text, ParentId) -> gen_server:call(?MODULE, {submenu, Id, Text, ParentId}).
-spec menuItem(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
menuItem(Id, Text, MenuId) -> gen_server:call(?MODULE, {menu_item, Id, Text, MenuId}).
-spec menuSeparator(binary(), binary()) -> ok | {error, Reason :: string()}.
menuSeparator(Id, MenuId) -> gen_server:call(?MODULE, {menu_separator, Id, MenuId}).
-spec menuBar(binary()) -> ok | {error, Reason :: string()}.
menuBar(Id) -> gen_server:call(?MODULE, {menu_bar, Id}).
-spec popupMenu(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
popupMenu(MenuId, FormId, WidgetId) -> gen_server:call(?MODULE, {popup_menu, MenuId, FormId, WidgetId}).
-spec popupMenu(binary(), binary()) -> ok | {error, Reason :: string()}.
popupMenu(MenuId, WidgetId) -> popupMenu(MenuId, ?EPONA_MAIN_WINDOW, WidgetId).

-spec toBoxLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
toBoxLayout(FormId, Id, LayoutId) -> gen_server:call(?MODULE, {to_box_layout, FormId, Id, LayoutId}).
-spec toBoxLayout(binary(), binary()) -> ok | {error, Reason :: string()}.
toBoxLayout(Id, LayoutId) -> toBoxLayout(?EPONA_MAIN_WINDOW, Id, LayoutId).
-spec toFormLayout(binary(), binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
toFormLayout(FormId, LabelId, FieldId, LayoutId) ->
	gen_server:call(?MODULE, {to_form_layout, FormId, LabelId, FieldId, LayoutId}).
-spec toFormLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
toFormLayout(LabelId, FieldId, LayoutId) -> toFormLayout(?EPONA_MAIN_WINDOW, LabelId, FieldId, LayoutId).
-spec toGridLayout(binary(), binary(), binary(), non_neg_integer(), non_neg_integer()) -> ok | {error, Reason :: string()}.
toGridLayout(FormId, Id, LayoutId, Row, Column) ->
	gen_server:call(?MODULE, {to_grid_layout, FormId, Id, LayoutId, Row, Column}).
-spec toGridLayout(binary(), binary(), non_neg_integer(), non_neg_integer()) -> ok | {error, Reason :: string()}.
toGridLayout(Id, LayoutId, Row, Column) -> toGridLayout(?EPONA_MAIN_WINDOW, Id, LayoutId, Row, Column).
-spec toStackedLayout(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
toStackedLayout(FormId, Id, LayoutId) -> gen_server:call(?MODULE, {to_stacked_layout, FormId, Id, LayoutId}).
-spec toStackedLayout(binary(), binary()) -> ok | {error, Reason :: string()}.
toStackedLayout(Id, LayoutId) -> toStackedLayout(?EPONA_MAIN_WINDOW, Id, LayoutId).
-spec toTabWidget(binary(), binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
toTabWidget(FormId, Id, TabWidgetId, Label) ->
	gen_server:call(?MODULE, {to_tab_widget, FormId, Id, TabWidgetId, Label}).
-spec toTabWidget(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
toTabWidget(Id, TabWidgetId, Label) -> toTabWidget(?EPONA_MAIN_WINDOW, Id, TabWidgetId, Label).
-spec toSplitter(binary(), binary(), binary()) -> ok | {error, Reason :: string()}.
toSplitter(FormId, Id, SplitterId) ->
	gen_server:call(?MODULE, {to_splitter, FormId, Id, SplitterId}).
-spec toSplitter(binary(), binary()) -> ok | {error, Reason :: string()}.
toSplitter(Id, SplitterId) -> toSplitter(?EPONA_MAIN_WINDOW, Id, SplitterId).

-spec property(id(), id(), {binary(), binary() | string()}) -> ok | {error, Reason :: string()}.
property(FormId, Id, {PropertyName, PropertyValue}) when is_list(PropertyValue) ->
	property(FormId, Id, {PropertyName, list_to_binary(PropertyValue)});
property(FormId, Id, {PropertyName, PropertyValue}) ->
	gen_server:call(?MODULE, {set_property, FormId, Id, {PropertyName, PropertyValue}}).
-spec property(id(), {binary(), binary() | string()}) -> ok | {error, Reason :: string()}.
property(Id, {PropertyName, PropertyValue}) -> property(?EPONA_MAIN_WINDOW, Id, {PropertyName, PropertyValue}).

-spec properties(id(), id(), [{binary(), binary()}]) -> [ok | {error, Reason :: string()}].
properties(FormId, Id, Properties) ->
	Fun =
		fun({PropertyName, PropertyValue}) ->
			gen_server:call(?MODULE, {set_property, FormId, Id, {PropertyName, PropertyValue}})
		end,
	lists:map(Fun, Properties).
-spec properties(id(), [{binary(), binary()}]) -> [ok | {error, Reason :: string()}].
properties(Id, Properties) ->	properties(?EPONA_MAIN_WINDOW, Id, Properties).

-spec resize(binary(), non_neg_integer(), non_neg_integer()) -> ok | {error, Reason :: string()}.
resize(FormId, Width, Height) -> gen_server:call(?MODULE, {resize, FormId, Width, Height}).
-spec resize(non_neg_integer(), non_neg_integer()) -> ok | {error, Reason :: string()}.
resize(Width, Height) -> resize(?EPONA_MAIN_WINDOW, Width, Height).
-spec maximize(binary()) -> ok | {error, Reason :: string()}.
maximize(FormId) -> gen_server:call(?MODULE, {maximize, FormId}).
-spec close() -> ok | {error, Reason :: string()}.
close() -> gen_server:call(?MODULE, close).

-spec subscribe(pid(), binary()) -> ok | {error, Reason :: string()}.
subscribe(ClientPid, Id) -> gen_server:call(?MODULE, {subscribe, ClientPid, Id}).
%TODO ? unsubscribe(ClientPid, ControlId) -> gen_server:call(?MODULE, {subscribe, ClientPid, ControlId}).

-spec batch(list()) -> ok.
batch(List) -> forEach(List).
-spec newId() -> binary().
newId() ->
	{MegaSecs, Secs, MicroSecs} = erlang:timestamp(),
	iolist_to_binary([<<"id">>, integer_to_binary(MegaSecs), integer_to_binary(Secs), integer_to_binary(MicroSecs)]).
-spec idToLayoutId(binary()) -> binary().
idToLayoutId(Id) -> <<<<"layout_">>/binary, Id/binary>>.


%% Callbacks

init([]) ->
	io:format("epona:init/1: ~p~n", [self()]),
	% Find and run our port.
	PortEnv = "QT5_EPORT_DIR",
	Port = case os:getenv(PortEnv) of
				 false ->
					 io:format("epona:init/1: unable to find executable in: ~p~n", [PortEnv]),
					 undefined;
				 PortPath ->
					 Cmd = PortPath ++ "/qt5_eport.bin",
					 io:format("epona:init/1: executable is: ~p~n", [Cmd]),
					 open_port({spawn, Cmd}, [{line, 4096}, use_stdio, exit_status])
			 end,
	% The state, finally.
	{
		ok,
		#state
		{
			qt_port = Port
		}
	}.

handle_call({form, Id, Title}, _From, State = #state{qt_port = Port}) ->
	form(Port, Id, Title),
	{reply, ok, State};

handle_call({information_dialog, Title, Text}, _From, State = #state{qt_port = Port}) ->
	informationDialog(Port, Title, Text),
	{reply, ok, State};
handle_call({warning_dialog, Title, Text}, _From, State = #state{qt_port = Port}) ->
	warningDialog(Port, Title, Text),
	{reply, ok, State};
handle_call({question_dialog, ClientPid, RequestId, Title, Text}, _From, State = #state{qt_port = Port, clients = Clients}) ->
	questionDialog(Port, ClientPid, RequestId, Title, Text),
	NewClients = Clients#{RequestId => ClientPid},
	{reply, ok, State#state{clients = NewClients}};

handle_call({horizontal_layout, FormId, Id, ParentId}, _From, State = #state{qt_port = Port}) ->
	makeLayout(?EPONA_HORIZONTAL_LAYOUT, Port, FormId, Id, ParentId),
	{reply, ok, State};
handle_call({vertical_layout, FormId, Id, ParentId}, _From, State = #state{qt_port = Port}) ->
	makeLayout(?EPONA_VERTICAL_LAYOUT, Port, FormId, Id, ParentId),
	{reply, ok, State};
handle_call({form_layout, FormId, Id, ParentId}, _From, State = #state{qt_port = Port}) ->
	makeLayout(?EPONA_FORM_LAYOUT, Port, FormId, Id, ParentId),
	{reply, ok, State};
handle_call({grid_layout, FormId, Id, ParentId}, _From, State = #state{qt_port = Port}) ->
	makeLayout(?EPONA_GRID_LAYOUT, Port, FormId, Id, ParentId),
	{reply, ok, State};
handle_call({stacked_layout, FormId, Id, ParentId}, _From, State = #state{qt_port = Port}) ->
	makeLayout(?EPONA_STACKED_LAYOUT, Port, FormId, Id, ParentId),
	{reply, ok, State};

handle_call({stretcher, LayoutId}, _From, State = #state{qt_port = Port}) ->
	addStretcher(Port, LayoutId),
	{reply, ok, State};

handle_call({check_box, Id, Text}, _From, State = #state{qt_port = Port}) ->
	checkBox(Port, Id, Text),
	{reply, ok, State};
handle_call({combo_box, Id, Items}, _From, State = #state{qt_port = Port}) ->
	comboBox(Port, Id, Items),
	{reply, ok, State};
handle_call({group_box, Id, Title, LayoutType}, _From, State = #state{qt_port = Port}) ->
	groupBox(Port, Id, Title, LayoutType),
	{reply, ok, State};
handle_call({label, Id, Text}, _From, State = #state{qt_port = Port}) ->
	label(Port, Id, Text),
	{reply, ok, State};
handle_call({line_edit, Id, Text}, _From, State = #state{qt_port = Port}) ->
	lineEdit(Port, Id, Text),
	{reply, ok, State};
handle_call({list_widget, Id, Items}, _From, State = #state{qt_port = Port}) ->
	listWidget(Port, Id, Items),
	{reply, ok, State};
handle_call({push_button, Id, Text}, _From, State = #state{qt_port = Port}) ->
	pushButton(Port, Id, Text),
	{reply, ok, State};
handle_call({radio_button, Id, Text}, _From, State = #state{qt_port = Port}) ->
	radioButton(Port, Id, Text),
	{reply, ok, State};
handle_call({splitter, Id, Orientation}, _From, State = #state{qt_port = Port}) ->
	splitter(Port, Id, Orientation),
	{reply, ok, State};
handle_call({tab_widget, Id}, _From, State = #state{qt_port = Port}) ->
	tabWidget(Port, Id),
	{reply, ok, State};
handle_call({widget, Id, LayoutType}, _From, State = #state{qt_port = Port}) ->
	widget(Port, Id, LayoutType),
	{reply, ok, State};

handle_call({menu, Id}, _From, State = #state{qt_port = Port}) ->
	menu(Port, Id),
	{reply, ok, State};
handle_call({submenu, Id, Text, ParentId}, _From, State = #state{qt_port = Port}) ->
	submenu(Port, Id, Text, ParentId),
	{reply, ok, State};
handle_call({menu_item, Id, Text, MenuId}, _From, State = #state{qt_port = Port}) ->
	menuItem(Port, Id, Text, MenuId),
	{reply, ok, State};
handle_call({menu_separator, Id, MenuId}, _From, State = #state{qt_port = Port}) ->
	menuSeparator(Port, Id, MenuId),
	{reply, ok, State};
handle_call({menu_bar, Id}, _From, State = #state{qt_port = Port}) ->
	menuBar(Port, Id),
	{reply, ok, State};
handle_call({popup_menu, MenuId, FormId, WidgetId}, _From, State = #state{qt_port = Port}) ->
	popupMenu(Port, MenuId, FormId, WidgetId),
	{reply, ok, State};

handle_call({to_box_layout, FormId, Id, LayoutId}, _From, State = #state{qt_port = Port}) ->
	toBoxLayout(Port, FormId, Id, LayoutId),
	{reply, ok, State};
handle_call({to_form_layout, FormId, LabelId, FieldId, LayoutId}, _From, State = #state{qt_port = Port}) ->
	toFormLayout(Port, FormId, LabelId, FieldId, LayoutId),
	{reply, ok, State};
handle_call({to_grid_layout, FormId, Id, LayoutId, Row, Column}, _From, State = #state{qt_port = Port}) ->
	toGridLayout(Port, FormId, Id, LayoutId, Row, Column),
	{reply, ok, State};
handle_call({to_stacked_layout, FormId, Id, LayoutId}, _From, State = #state{qt_port = Port}) ->
	toStackedLayout(Port, FormId, Id, LayoutId),
	{reply, ok, State};
handle_call({to_tab_widget, FormId, Id, TabWidgetId, Label}, _From, State = #state{qt_port = Port}) ->
	toTabWidget(Port, FormId, Id, TabWidgetId, Label),
	{reply, ok, State};
handle_call({to_splitter, FormId, Id, SplitterId}, _From, State = #state{qt_port = Port}) ->
	toSplitter(Port, FormId, Id, SplitterId),
	{reply, ok, State};

handle_call({set_property, FormId, Id, {PropertyName, PropertyValue}}, _From, State = #state{qt_port = Port}) ->
	setProperty(Port, FormId, Id, {PropertyName, PropertyValue}),
	{reply, ok, State};
handle_call({resize, FormId, Width, Height}, _From, State = #state{qt_port = Port}) ->
	resize(Port, FormId, Width, Height),
	{reply, ok, State};
handle_call({maximize, FormId}, _From, State = #state{qt_port = Port}) ->
	maximize(Port, FormId),
	{reply, ok, State};
handle_call(close, _From, State = #state{qt_port = Port}) ->
	close(Port),
	{reply, ok, State};

handle_call({subscribe, ClientPid, Id}, _From, State = #state{subscribers = Subscribers}) ->
	NewSubscribers = Subscribers#{Id => ClientPid},
	{reply, ok, State#state{subscribers = NewSubscribers}};

handle_call(_Request, _From, State = #state{}) ->
	{reply, ok, State}.

handle_cast(_Request, State = #state{}) -> {noreply, State}.

handle_info({_Port, {data, {_, Text}}}, State) ->
	% A notification from qt_port.
	{noreply, processResponse(Text, State)};
handle_info({_Port, {exit_status, _}}, State) ->
	%TODO Shut app down.
	{noreply, State};
handle_info(Info, State = #state{}) ->
	io:format("handle_info: ~p~n", [Info]),
	{noreply, State}.

terminate(_Reason, _State = #state{}) -> ok.


%% UI building blocks

%% Forms

form(Port, Id, Title) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TITLE => Title
		}),
	requestPort(Port, "$form " ++ binary_to_list(Json)).


%% Interactions

informationDialog(Port, Title, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_TITLE => list_to_binary(Title),
			?EPONA_TEXT => list_to_binary(Text)
		}),
	requestPort(Port, "$informationDialog " ++ binary_to_list(Json)).

warningDialog(Port, Title, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_TITLE => list_to_binary(Title),
			?EPONA_TEXT => list_to_binary(Text)
		}),
	requestPort(Port, "$warningDialog " ++ binary_to_list(Json)).

questionDialog(Port, _ClientId, RequestId, Title, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => RequestId,
			?EPONA_TITLE => list_to_binary(Title),
			?EPONA_TEXT => list_to_binary(Text)
		}),
	requestPort(Port, "$questionDialog " ++ binary_to_list(Json)).

%% Layouts

makeLayout(Type, Port, FormId, Id, ParentId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_ID => Id,
			?EPONA_PARENT_ID => ParentId
		}),
	requestPort(Port, Type ++ " " ++ binary_to_list(Json)).

addStretcher(Port, LayoutId) ->
	{ok, Json} = mapToJson(
		#{
%%			?EPONA_FORM_ID => FormId,
			?EPONA_LAYOUT_ID => LayoutId
		}),
	requestPort(Port, "$stretcher " ++ binary_to_list(Json)).


%% Widgets

checkBox(Port, Id, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TEXT => Text
		}),
	requestPort(Port, "$checkBox " ++ binary_to_list(Json)).

comboBox(Port, Id, Items) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_ITEMS => Items
		}),
	requestPort(Port, "$comboBox " ++ binary_to_list(Json)).

groupBox(Port, Id, Title, LayoutType) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TITLE => Title
		}),
	requestPort(Port, "$groupBox " ++ binary_to_list(Json)),
	probablyAttachLayout(LayoutType, Port, Id).

label(Port, Id, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TEXT => Text
		}),
	requestPort(Port, "$label " ++ binary_to_list(Json)).

lineEdit(Port, Id, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TEXT => Text
		}),
	requestPort(Port, "$lineEdit " ++ binary_to_list(Json)).

listWidget(Port, Id, Items) ->
	%TODO Handle string()/binary() case?
	%%	BinaryItems = lists:map(fun(Item) -> list_to_binary(Item) end, Items),
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_ITEMS => Items
		}),
	requestPort(Port, "$listWidget " ++ binary_to_list(Json)).

pushButton(Port, Id, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TEXT => Text
		}),
	requestPort(Port, "$pushButton " ++ binary_to_list(Json)).

radioButton(Port, Id, Text) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TEXT => Text
		}),
	requestPort(Port, "$radioButton " ++ binary_to_list(Json)).

splitter(Port, Id, Orientation) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_ORIENTATION => atom_to_binary(Orientation, latin1)
		}),
	requestPort(Port, "$splitter " ++ binary_to_list(Json)).

tabWidget(Port, Id) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id
		}),
	requestPort(Port, "$tabWidget " ++ binary_to_list(Json)).

widget(Port, Id, LayoutType) ->
	% First create the widget itself.
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id
		}),
	requestPort(Port, "$widget " ++ binary_to_list(Json)),
	% Now check if we need to auto-attach a layout to widget.
	probablyAttachLayout(LayoutType, Port, Id).

probablyAttachLayout(horizontal_layout, Port, Id) ->
	makeLayout(?EPONA_HORIZONTAL_LAYOUT, Port, ?EPONA_MAIN_WINDOW, idToLayoutId(Id), Id);
probablyAttachLayout(vertical_layout, Port, Id) ->
	makeLayout(?EPONA_VERTICAL_LAYOUT, Port, ?EPONA_MAIN_WINDOW, idToLayoutId(Id), Id);
probablyAttachLayout(form_layout, Port, Id) ->
	makeLayout(?EPONA_FORM_LAYOUT, Port, ?EPONA_MAIN_WINDOW, idToLayoutId(Id), Id);
probablyAttachLayout(grid_layout, Port, Id) ->
	makeLayout(?EPONA_GRID_LAYOUT, Port, ?EPONA_MAIN_WINDOW, idToLayoutId(Id), Id);
probablyAttachLayout(stacked_layout, Port, Id) ->
	makeLayout(?EPONA_STACKED_LAYOUT, Port, ?EPONA_MAIN_WINDOW, idToLayoutId(Id), Id);
probablyAttachLayout(no_layout, _Port, _Id) -> ok;
probablyAttachLayout(Unknown, _Port, _Id) ->
	io:format("Unknown layout type requested: ~p~n", [Unknown]).


menu(Port, Id) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id
		}),
	requestPort(Port, "$menu " ++ binary_to_list(Json)).

submenu(Port, Id, Text, ParentId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TEXT => Text,
			?EPONA_PARENT_ID => ParentId
		}),
	requestPort(Port, "$submenu " ++ binary_to_list(Json)).

menuItem(Port, Id, Text, MenuId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_TEXT => Text,
			?EPONA_MENU_ID => MenuId
		}),
	requestPort(Port, "$menuItem " ++ binary_to_list(Json)).

menuSeparator(Port, Id, MenuId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id,
			?EPONA_MENU_ID => MenuId
		}),
	requestPort(Port, "$menuSeparator " ++ binary_to_list(Json)).

menuBar(Port, Id) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_ID => Id
		}),
	requestPort(Port, "$menuBar " ++ binary_to_list(Json)).

popupMenu(Port, MenuId, FormId, WidgetId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_MENU_ID => MenuId,
			?EPONA_FORM_ID => FormId,
			?EPONA_WIDGET_ID => WidgetId
		}),
	requestPort(Port, "$popupMenu " ++ binary_to_list(Json)).


%% Composition

toBoxLayout(Port, FormId, Id, LayoutId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_ID => Id,
			?EPONA_LAYOUT_ID => LayoutId
		}),
	requestPort(Port, "$toBoxLayout " ++ binary_to_list(Json)).

toFormLayout(Port, FormId, LabelId, FieldId, LayoutId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_LABEL_ID => LabelId,
			?EPONA_FIELD_ID => FieldId,
			?EPONA_LAYOUT_ID => LayoutId
		}),
	requestPort(Port, "$toFormLayout " ++ binary_to_list(Json)).

toGridLayout(Port, FormId, Id, LayoutId, Row, Column) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_ID => Id,
			?EPONA_LAYOUT_ID => LayoutId,
			?EPONA_ROW => Row,
			?EPONA_COLUMN => Column
		}),
	requestPort(Port, "$toGridLayout " ++ binary_to_list(Json)).

toStackedLayout(Port, FormId, Id, LayoutId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_ID => Id,
			?EPONA_LAYOUT_ID => LayoutId
		}),
	requestPort(Port, "$toStackedLayout " ++ binary_to_list(Json)).

toTabWidget(Port, FormId, Id, TabWidgetId, Label) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_ID => Id,
			?EPONA_TAB_WIDGET_ID => TabWidgetId,
			?EPONA_LABEL => Label
		}),
	requestPort(Port, "$toTabWidget " ++ binary_to_list(Json)).

toSplitter(Port, FormId, Id, SplitterId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_ID => Id,
			?EPONA_SPLITTER_ID => SplitterId
		}),
	requestPort(Port, "$toSplitter " ++ binary_to_list(Json)).


%% Properties

setProperty(Port, FormId, Id, {PropertyName, PropertyValue}) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_ID => Id,
			?EPONA_NAME => PropertyName,
			?EPONA_VALUE => PropertyValue
		}),
	requestPort(Port, "$setProperty " ++ binary_to_list(Json)).

resize(Port, FormId, Width, Height) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId,
			?EPONA_WIDTH => Width,
			?EPONA_HEIGHT => Height
		}),
	requestPort(Port, "$resize " ++ binary_to_list(Json)).

maximize(Port, FormId) ->
	{ok, Json} = mapToJson(
		#{
			?EPONA_FORM_ID => FormId
		}),
	requestPort(Port, "$maximize " ++ binary_to_list(Json)).

close(Port) ->
	%TODO Do we need some special handling here?
	requestPort(Port, "$close").


%% Response processing

processResponse(Text, State) when is_binary(Text) ->
	processResponse(binary_to_list(Text), State);
processResponse(Text, State) ->
	[Response, Json] = re:split(Text, " ", [{parts, 2}]),
	case Response of
		?EPONA_QUESTION_RESPONSE -> processQuestionResponse(Json, State);
		?EPONA_BUTTON_CLICKED -> processButtonEvent(?EPONA_BUTTON_CLICKED, Json, State);
		?EPONA_BUTTON_PRESSED -> processButtonEvent(?EPONA_BUTTON_PRESSED, Json, State);
		?EPONA_BUTTON_RELEASED -> processButtonEvent(?EPONA_BUTTON_RELEASED, Json, State);
		?EPONA_BUTTON_TOGGLED -> processButtonToggled(Json, State);
		?EPONA_BUTTON_RIGHT_PRESSED -> processButtonEvent(?EPONA_BUTTON_RIGHT_PRESSED, Json, State);
		?EPONA_CHECK_BOX_STATE_CHANGED -> processCheckBoxStateChanged(Json, State);
		?EPONA_COMBO_BOX_CURRENT_INDEX_CHANGED -> processComboBoxIndexChanged(Json, State);
		?EPONA_COMBO_BOX_CURRENT_TEXT_CHANGED -> processComboBoxTextChanged(Json, State);
		?EPONA_COMBO_BOX_EDIT_TEXT_CHANGED -> processComboBoxEditTextChanged(Json, State);
		?EPONA_LINE_EDIT_TEXT_EDITED -> processLineEditTextEdited(Json, State);
		?EPONA_TAB_WIDGET_CURRENT_CHANGED -> processTabWidgetEvent(?EPONA_TAB_WIDGET_CURRENT_CHANGED, Json, State);
		?EPONA_TAB_WIDGET_TAB_BAR_CLICKED -> processTabWidgetEvent(?EPONA_TAB_WIDGET_TAB_BAR_CLICKED, Json, State);
		?EPONA_TAB_WIDGET_TAB_BAR_DOUBLE_CLICKED ->
			processTabWidgetEvent(?EPONA_TAB_WIDGET_TAB_BAR_DOUBLE_CLICKED, Json, State);
		?EPONA_TAB_WIDGET_TAB_CLOSE_REQUESTED ->
			processTabWidgetEvent(?EPONA_TAB_WIDGET_TAB_CLOSE_REQUESTED, Json, State);
		?EPONA_MENU_TRIGGERED -> processMenuTriggered(Json, State)
	end.

processQuestionResponse(Json, State = #state{clients = Clients}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := RequestId,
		?EPONA_VALUE := Value
	} = Map,
	case maps:get(RequestId, Clients, nil) of
		nil -> State;
		ClientId ->
			ClientId ! {?EPONA_QUESTION_RESPONSE, RequestId, Value},
			State#state{clients = maps:remove(RequestId, Clients)}
	end.

processButtonEvent(Event, Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id
	} = Map,
	notifySubscriberOf(Id, {Event, Id}, Subscribers),
	State.

processButtonToggled(Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id,
		?EPONA_CHECKED := Checked
	} = Map,
	notifySubscriberOf(Id, {?EPONA_BUTTON_TOGGLED, Id, Checked}, Subscribers),
	State.

processCheckBoxStateChanged(Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id,
		?EPONA_STATE := CBState
	} = Map,
	notifySubscriberOf(Id, {?EPONA_CHECK_BOX_STATE_CHANGED, Id, binary_to_integer(CBState)}, Subscribers),
	State.

processComboBoxIndexChanged(Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id,
		?EPONA_INDEX := Index
	} = Map,
	notifySubscriberOf(Id, {?EPONA_COMBO_BOX_CURRENT_INDEX_CHANGED, Id, binary_to_integer(Index)}, Subscribers),
	State.

processComboBoxTextChanged(Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id,
		?EPONA_TEXT := Text
	} = Map,
	notifySubscriberOf(Id, {?EPONA_COMBO_BOX_CURRENT_TEXT_CHANGED, Id, binary_to_list(Text)}, Subscribers),
	State.

processComboBoxEditTextChanged(Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id,
		?EPONA_TEXT := Text
	} = Map,
	notifySubscriberOf(Id, {?EPONA_COMBO_BOX_EDIT_TEXT_CHANGED, Id, binary_to_list(Text)}, Subscribers),
	State.

processLineEditTextEdited(Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id,
		?EPONA_TEXT := Text
	} = Map,
	notifySubscriberOf(Id, {?EPONA_LINE_EDIT_TEXT_EDITED, Id, binary_to_list(Text)}, Subscribers),
	State.

processTabWidgetEvent(Event, Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id,
		?EPONA_INDEX := Index
	} = Map,
	notifySubscriberOf(Id, {Event, Id, binary_to_integer(Index)}, Subscribers),
	State.

processMenuTriggered(Json, State = #state{subscribers = Subscribers}) ->
	{ok, Map} = jsonToMap(Json),
	#{
		?EPONA_ID := Id
	} = Map,
	notifySubscriberOf(Id, {?EPONA_MENU_TRIGGERED, Id}, Subscribers),
	State.


%% Utilities

-spec forEach([any()]) -> ok.
forEach(List) ->
	%TODO Comment it out, explaing everything.
	Fun =
		fun
			({Function, Args}) ->
				erlang:apply(?MODULE, Function, Args);
			({Function, Args, {ComposeFunction, ComposeArgs}}) ->
				erlang:apply(?MODULE, Function, Args),
				erlang:apply(?MODULE, ComposeFunction, [hd(Args) | ComposeArgs])
		end,
	lists:foreach(Fun, List).

requestPort(Port, Request) ->
	% A single request to qt_port.
%%	io:format("epona:requestPort: ~p~n", [Request]),
	erlang:port_command(Port, Request ++ "\n").

jsonToMap(Data) ->
	try
		{ok, jiffy:decode(Data, [return_maps])}
	catch
		_:_ ->
			io:format("NOT decoded: bad json: ~p~n", [Data]),
			{error, bad_json}
	end.

mapToJson(Map) ->
	try
		{ok, jiffy:encode(Map)}
	catch
		_:_ ->
			io:format("NOT encoded: bad map: ~p~n", [Map]),
			{error, bad_map}
	end.

notifySubscriberOf(Id, Message, Subscribers) ->
	%TODO Should we keep more than one subscriber?
	case maps:get(Id, Subscribers, nil) of
		nil -> ok;
		SubscriberPid ->
			SubscriberPid ! Message
	end.


%% TODOs

%%QCalendarWidget
%%QColorDialog
%%QDateEdit
%%QDateTimeEdit
%%QDial
%%QDoubleSpinBox
%%QFileDialog
%%QFontComboBox
%%QFontDialog
%%QGraphicsView
%%QInputDialog
%%QLCDNumber
%%QListView
%%QOpenGLWidget
%%QPlainTextEdit
%%QProgressBar
%%QProgressDialog
%%QScrollArea
%%QScrollBar
%%QSlider
%%QSpinBox
%%QStatusBar
%%QSvgWidget
%%QTabBar
%%QTableView
%%QTextEdit
%%QTimeEdit
%%QToolBar
%%QToolBox
%%QToolButton
%%QTreeView

