-author("ra").

%% Events

-define(EPONA_QUESTION_RESPONSE, <<"$questionResponse">>).

-define(EPONA_BUTTON_CLICKED, <<"$buttonClicked">>).
-define(EPONA_BUTTON_PRESSED, <<"$buttonPressed">>).
-define(EPONA_BUTTON_RELEASED, <<"$buttonReleased">>).
-define(EPONA_BUTTON_TOGGLED, <<"$buttonToggled">>).
-define(EPONA_BUTTON_RIGHT_PRESSED, <<"$buttonRightPressed">>).
-define(EPONA_CHECK_BOX_STATE_CHANGED, <<"$checkBoxStateChanged">>).
-define(EPONA_COMBO_BOX_CURRENT_INDEX_CHANGED, <<"$comboBoxCurrentIndexChanged">>).
-define(EPONA_COMBO_BOX_CURRENT_TEXT_CHANGED, <<"$comboBoxCurrentTextChanged">>).
-define(EPONA_COMBO_BOX_EDIT_TEXT_CHANGED, <<"$comboBoxEditTextChanged">>).
-define(EPONA_LINE_EDIT_TEXT_EDITED, <<"$lineEditTextEdited">>).
-define(EPONA_TAB_WIDGET_CURRENT_CHANGED, <<"$tabWidgetCurrentChanged">>).
-define(EPONA_TAB_WIDGET_TAB_BAR_CLICKED, <<"$tabWidgetTabBarClicked">>).
-define(EPONA_TAB_WIDGET_TAB_BAR_DOUBLE_CLICKED, <<"$tabWidgetTabBarDoubleClicked">>).
-define(EPONA_TAB_WIDGET_TAB_CLOSE_REQUESTED, <<"$tabWidgetTabCloseRequested">>).
-define(EPONA_MENU_TRIGGERED, <<"$menuTriggered">>).

%% Values

-define(EPONA_TRUE, <<"true">>).
-define(EPONA_FALSE, <<"false">>).
-define(EPONA_OK, <<"ok">>).
-define(EPONA_CANCEL, <<"cancel">>).
-define(EPONA_YES, <<"yes">>).
-define(EPONA_NO, <<"no">>).

%% Identifications

-define(EPONA_ID, <<"id">>).
-define(EPONA_FORM_ID, <<"form_id">>).
-define(EPONA_WIDGET_ID, <<"widget_id">>).
-define(EPONA_LAYOUT_ID, <<"layout_id">>).
-define(EPONA_PARENT_ID, <<"parent_id">>).
-define(EPONA_LABEL_ID, <<"label_id">>).
-define(EPONA_FIELD_ID, <<"field_id">>).
-define(EPONA_TAB_WIDGET_ID, <<"tab_widget_id">>).
-define(EPONA_SPLITTER_ID, <<"splitter_id">>).
-define(EPONA_MENU_ID, <<"menu_id">>).
-define(EPONA_VALUE, <<"value">>).
-define(EPONA_TITLE, <<"title">>).
-define(EPONA_TEXT, <<"text">>).
-define(EPONA_ITEMS, <<"items">>).
-define(EPONA_NAME, <<"name">>).
-define(EPONA_TYPE, <<"type">>).
-define(EPONA_INDEX, <<"index">>).
-define(EPONA_ROW, <<"row">>).
-define(EPONA_COLUMN, <<"column">>).
-define(EPONA_LABEL, <<"label">>).
-define(EPONA_ORIENTATION, <<"orientation">>).

%% Predefined controls

-define(EPONA_EMPTY, <<"">>).
-define(EPONA_NO_LAYOUT, <<"">>).
-define(EPONA_MAIN_WINDOW, <<"MainWindow">>).

%% Properties

-define(EPONA_WINDOW_TITLE, <<"windowTitle">>).
-define(EPONA_SPACING, <<"spacing">>).
-define(EPONA_HORIZONTAL_SPACING, <<"horizontalSpacing">>).
-define(EPONA_VERTICAL_SPACING, <<"verticalSpacing">>).
-define(EPONA_FLAT, <<"flat">>).
-define(EPONA_CHECKED, <<"checked">>).
-define(EPONA_STATE, <<"state">>).
-define(EPONA_WIDTH, <<"width">>).
-define(EPONA_HEIGHT, <<"height">>).
-define(EPONA_CURRENT_INDEX, <<"currentIndex">>).
-define(EPONA_FRAME_SHAPE, <<"frameShape">>).
-define(EPONA_FOCUS_POLICY, <<"focusPolicy">>).
-define(EPONA_STYLE_SHEET, <<"styleSheet">>).

%% General commands

-define(EPONA_HORIZONTAL_LAYOUT, "$horizontalLayout").
-define(EPONA_VERTICAL_LAYOUT, "$verticalLayout").
-define(EPONA_FORM_LAYOUT, "$formLayout").
-define(EPONA_GRID_LAYOUT, "$gridLayout").
-define(EPONA_STACKED_LAYOUT, "$stackedLayout").

