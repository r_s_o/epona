-module(minesweeper).
-author("ra").
-behaviour(gen_server).
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2]).
-include("ids.hrl").

-define(H, 20).
-define(W, 30).

-define(MAIN_LAYOUT, <<"mainLayout">>).
-define(GRID, <<"grid">>).
-define(MAIN_WIDGET, <<"tabs">>).
-define(TAB_GRID, <<"tabGrid">>).
-define(GRID_LAYOUT, epona:idToLayoutId(?TAB_GRID)).
-define(TAB_RULES, <<"tabRules">>).
-define(RULES_LAYOUT, epona:idToLayoutId(?TAB_RULES)).
-define(TAB_ABOUT, <<"tabAbout">>).
-define(ABOUT_LAYOUT, epona:idToLayoutId(?TAB_ABOUT)).

-type array() :: array:array().
-type coord() :: {integer(), integer()}.
-type sign() :: unmarked | exclamation | question | cleared.
-type signs() :: #{coord() => sign()}.

-record(state,
{
	field :: array(),
	signs = #{} :: signs()
}).
-type state() :: #state{}.


%% API

start_link() ->
	io:format("minesweeper:start_link~n"),
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


%% Callbacks

init([]) ->
	gen_server:cast(?MODULE, build_ui), % Defer.
	Matrix = array:new([{size, ?H}, {default, array:new(?W)}]),
	{
		ok,
		#state
		{
			field = populate(Matrix)
		}
	}.

handle_call(_Request, _From, State = #state{}) -> {reply, ok, State}.

handle_cast(build_ui, State = #state{}) ->
	{noreply, buildUi(State)};
handle_cast(_Request, State = #state{}) ->
	{noreply, State}.

handle_info({?EPONA_BUTTON_CLICKED, <<"b", Row:2/binary, Col:2/binary>>}, State = #state{}) ->
	{noreply, leftClicked(binary_to_integer(Row), binary_to_integer(Col), State)};
handle_info({?EPONA_BUTTON_RIGHT_PRESSED, <<"b", Row:2/binary, Col:2/binary>>}, State = #state{}) ->
	NewState = rightClicked(binary_to_integer(Row), binary_to_integer(Col), State),
	victory(NewState),
	{noreply, rightClicked(binary_to_integer(Row), binary_to_integer(Col), State)};
handle_info(lost, State = #state{}) ->
	RequestId = <<"lostRequest">>,
	epona:questionDialog(self(), RequestId, "Defeat", "You have lost. Try again?"),
	NewState = receive
					  {?EPONA_QUESTION_RESPONSE, _, ?EPONA_YES} ->
						  restart(State);
					  {?EPONA_QUESTION_RESPONSE, _, ?EPONA_NO} ->
						  epona:close(),
						  State
				  end,
	{noreply, NewState};
handle_info(won, State = #state{}) ->
	RequestId = <<"wonRequest">>,
	epona:questionDialog(self(), RequestId, "Victory", "You have won. Try again?"),
	NewState = receive
					  {?EPONA_QUESTION_RESPONSE, _, ?EPONA_YES} ->
						  restart(State);
					  {?EPONA_QUESTION_RESPONSE, _, ?EPONA_NO} ->
						  epona:close(),
						  State
				  end,
	{noreply, NewState};
handle_info(Info, State = #state{}) ->
	io:format(":: info ~p~n", [Info]),
	{noreply, State}.

terminate(_Reason, _State = #state{}) -> ok.


%% Utilities

-spec populate(array()) -> array().
populate(Field) ->
	ColFun = fun(_ColIndex, _Element) -> rand:uniform(10) > 6 end,
	RowFun = fun(_RowIndex, Row) -> array:map(ColFun, Row) end,
	array:map(RowFun, Field).

-spec restart(state()) -> state().
restart(State = #state{field = Field}) ->
	NewField = populate(Field),
	NewSigns = #{},
	% Real dumb approach :(
	Coords = [{Row, Col} || Row <- lists:seq(0, ?H - 1), Col <- lists:seq(0, ?W - 1)],
	Fun =
		fun({DRow, DCol}) ->
			Name = buttonName(DRow, DCol),
			epona:properties(Name,
				[
					{?EPONA_TEXT, countToSymbol(0)},
					{?EPONA_STYLE_SHEET, sheetInitial()},
					{<<"down">>, <<"false">>}
				])
		end,
	lists:foreach(Fun, Coords),
	State#state{field = NewField, signs = NewSigns}.

-spec get(array(), coord()) -> boolean() | undefined.
get(Field, {Row, Col}) when Row >= 0, Row < ?H, Col >= 0, Col < ?W ->
	array:get(Col, array:get(Row, Field));
get(_Field, {_Row, _Col}) -> undefined.

neibs8(Row, Col) ->
	Block =
		[
			{Dy, Dx} || Dy <- [Row - 1, Row, Row + 1], Dx <- [Col - 1, Col, Col + 1],
			Dy >= 0, Dx >= 0, Dy < ?H, Dx < ?W
		],
	lists:delete({Row, Col}, Block).

neibs4(Row, Col) ->
	[
		{Row - 1, Col},
		{Row + 1, Col},
		{Row, Col - 1},
		{Row, Col + 1}
	].

neibs8List(Field, Row, Col) ->
	lists:map
	(
		fun({DRow, DCol}) -> get(Field, {DRow, DCol}) end,
		neibs8(Row, Col)
	).

count(Field, Row, Col) ->
	Summator =
		fun(true, Count) -> Count + 1;
			(_Other, Count) -> Count
		end,
	lists:foldr(Summator, 0, neibs8List(Field, Row, Col)).

buttonName(Row, Col) ->
	list_to_binary(io_lib:format("b~2..0B~2..0B", [Row, Col])).

-spec buildUi(state()) -> state().
buildUi(State = #state{}) ->
	Ui =
		[
			% General MainWindow parameters.
			{property, [?EPONA_EMPTY, ?EPONA_MAIN_WINDOW, {?EPONA_WINDOW_TITLE, <<"Minesweeper">>}]},
			{resize, [800, 600]},
			{property, [?EPONA_MAIN_WINDOW, {<<"sizeConstraint">>, <<"QLayout::SetFixedSize">>}]},
			{horizontalLayout, [?MAIN_LAYOUT, ?EPONA_NO_LAYOUT]},
			{property, [?MAIN_LAYOUT, {?EPONA_SPACING, <<"0">>}]},
			{property, [?MAIN_LAYOUT, {<<"sizeConstraint">>, <<"QLayout::SetFixedSize">>}]},
			{tabWidget, [?MAIN_WIDGET], {toBoxLayout, [?MAIN_LAYOUT]}},

			% Main tab
			{widget, [?TAB_GRID, grid_layout], {toTabWidget, [?MAIN_WIDGET, <<"Game">>]}},
			{properties,
				[?GRID_LAYOUT, %TODO ?
					[
						{?EPONA_SPACING, <<"0">>},
						{?EPONA_HORIZONTAL_SPACING, <<"0">>},
						{?EPONA_VERTICAL_SPACING, <<"0">>},
						{<<"sizeConstraint">>, <<"QLayout::SetFixedSize">>}
					]
				]
			},

			% Rules tab
			{stretcher, [?RULES_LAYOUT]},
			{widget, [?TAB_RULES, vertical_layout], {toTabWidget, [?MAIN_WIDGET, <<"Rules">>]}},
			{label, [epona:newId(), <<"You know the rules.<br>And so do I.">>], {toBoxLayout, [?RULES_LAYOUT]}},
			{stretcher, [?RULES_LAYOUT]},

			% About tab
			{widget, [?TAB_ABOUT, vertical_layout], {toTabWidget, [?MAIN_WIDGET, <<"About">>]}},
			{label, [epona:newId(), <<"Yet Another Minesweeper Clone">>], {toBoxLayout, [?ABOUT_LAYOUT]}},
			{label, [epona:newId(), <<"in <s>colour</s> Erlang!">>], {toBoxLayout, [?ABOUT_LAYOUT]}},
			{stretcher, [?ABOUT_LAYOUT]},
			{label, [epona:newId(), <<"r.s.o.aliev@gmail.com<br>https://bitbucket.org/r_s_o/epona">>], {toBoxLayout, [?ABOUT_LAYOUT]}}
		] ++
		buttons() ++
		[
			{resize, [10, 10]}
		],
	epona:batch(Ui),
	State#state{}.

buttons() ->
	lists:flatten(
		lists:map(
			fun(Row) -> buttonsRow(Row) end,
			lists:seq(0, ?H - 1)
		)
	).

buttonsRow(Row) ->
	GenerateElements =
		fun(Col, Acc) ->
			Name = buttonName(Row, Col),
			Acc ++
			[
				{pushButton, [Name, <<" ">>], {toGridLayout, [?GRID_LAYOUT, Row, Col]}},
				{properties,
					[Name,
						[
							{?EPONA_FRAME_SHAPE, <<"QFrame::Box">>},
							{?EPONA_FOCUS_POLICY, <<"Qt::NoFocus">>},
							{?EPONA_STYLE_SHEET, sheetInitial()}
						]
					]
				},
				{subscribe, [self(), Name]}
			]
		end,
	lists:foldl(GenerateElements, [], lists:seq(0, ?W - 1)).

sheetInitial() ->
	<<"min-height: 32px;max-height: 32px;min-width: 32px;max-width: 32px; font: bold;">>.
sheetCleared(Count) ->
	Initial = sheetInitial(),
	Colour = case Count of
					0 -> <<"red;">>;
					1 -> <<"blue;">>;
					2 -> <<"green;">>;
					3 -> <<"#007777;">>;
					4 -> <<"#444488;">>;
					5 -> <<"#990000;">>;
					6 -> <<"#FF0000;">>;
					7 -> <<"#FF4444;">>;
					8 -> <<"#FF8888;">>
				end,
	<<Initial/binary, <<"background: transparent; color: ">>/binary, Colour/binary>>.

-spec leftClicked(integer(), integer(), state()) -> state().
leftClicked(Row, Col, State = #state{field = Field, signs = Signs}) ->
	Name = buttonName(Row, Col),
	case {get(Field, {Row, Col}), maps:get({Row, Col}, Signs, unmarked)} of
		{true, unmarked} -> % It's the mine!
			epona:property(Name, {?EPONA_TEXT, <<"*">>}),
			minesweeper ! lost,
			State;
		{false, unmarked} -> % Nah, empty.
			sweepArea(Row, Col, State);
		_ -> % It's marked already, do nothing.
			State
	end.

-spec rightClicked(integer(), integer(), state()) -> state().
rightClicked(Row, Col, State = #state{signs = Signs}) ->
	Name = buttonName(Row, Col),
	NewSigns =
		case maps:get({Row, Col}, Signs, unmarked) of
			unmarked -> % Not marked now => put the mine sign.
				epona:property(Name, {?EPONA_TEXT, <<"!">>}),
				Signs#{{Row, Col} => exclamation};
			exclamation -> % Mine-marked => do doubt.
				epona:property(Name, {?EPONA_TEXT, <<"?">>}),
				Signs#{{Row, Col} => question};
			question -> % Well, unmark.
				epona:property(Name, {?EPONA_TEXT, <<" ">>}),
				maps:remove({Row, Col}, Signs);
			cleared -> % Already opened, do nothing.
				Signs
		end,
	State#state{signs = NewSigns}.

-spec victory(state()) -> boolean().
victory(#state{signs = Signs}) ->
	ExclamationPred = fun(_Index, V) -> V == exclamation end,
	ClearedPred = fun(_Index, V) -> V == cleared end,
	Mines = maps:filter(ExclamationPred, Signs),
	Cleared = maps:filter(ClearedPred, Signs),
	case maps:size(Mines) + maps:size(Cleared) of
		?W * ?H ->
			minesweeper ! won,
			true;
		_ ->
			false
	end.

countToSymbol(0) -> <<" ">>;
countToSymbol(Count) -> integer_to_binary(Count).

-spec sweepArea(integer(), integer(), state()) -> state().
sweepArea(Row, Col, State = #state{field = Field, signs = Signs}) ->
	Area = flood(Field, Row, Col, #{}),
	VisualFun =
		fun({DRow, DCol}) ->
			Name = buttonName(DRow, DCol),
			Count = count(Field, DRow, DCol),
			epona:properties(Name,
				[
					{?EPONA_TEXT, countToSymbol(Count)},
					{?EPONA_STYLE_SHEET, sheetCleared(Count)},
					{<<"down">>, <<"true">>}
				])
		end,
	lists:foreach(VisualFun, maps:keys(Area)), % Visualize.
	SignFun =
		fun(Index = {_DRow, _DCol}, Acc = #{}) ->
			Acc#{Index => cleared}
		end,
	NewSigns = lists:foldl(SignFun, Signs, maps:keys(Area)),
	State#state{signs = NewSigns}.

-spec flood(array(), integer(), integer(), map()) -> map().
flood(Field, Row, Col, AreaBefore) ->
	Area = AreaBefore#{{Row, Col} => true}, % Mark current.
	Neibs = neibs4(Row, Col), % Incidentalities.
	Fun =
		fun(Index = {DRow, DCol}, Acc) ->
			% Empty & not marked yet.
			case {get(Field, {DRow, DCol}), maps:find(Index, Acc)} of
				{false, error} ->
					flood(Field, DRow, DCol, Acc); % We have to go deeper.
				_ ->
					Acc
			end
		end,
	lists:foldl(Fun, Area, Neibs).

